/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_sem.h

                      header file of C-Sim semaphores

*****************************************************************************/

#ifndef CSIM_SEM_H
#define CSIM_SEM_H 1

#include "csim.h"


/*
 * Structure that contains the state of a semaphore is defined bellow.
 * Usage example:
 * {
 *   CSIM_SEMAPHORE *sem;
 *   sem = s_new_semaphore(1);
 *   lock_sem(sem);
 *     ...
 *   unlock_sem(sem);
 *   dispose_semaphore(sem);
 * }
 */

/* Structure that contains the complete state of a semaphore. */
typedef struct csim_semaphore {
  csim_d_head;
  CSIM_UWORD count;
} CSIM_SEMAPHORE;


/*
 * Dynamicaly creates (allocates memory) and initializes a semaphore.
 * The cnt argument is the maximum number of simultanous locks on the
 * semaphore (typicaly 1).
 */
#define csim_new_semaphore(cnt)                                               \
  csim_init_semaphore((CSIM_SEMAPHORE *) malloc(sizeof(CSIM_SEMAPHORE)), cnt);

/* Frees all resources asociated with the semaphore. */
#define csim_dispose_semaphore(sem)                                           \
  csim_dispose_head((CSIM_HEAD *) sem);


/*
 * Initializes the given semaphore. This function must be used explicitly
 * if staticaly allocated semaphore is used.
 */
CSIM_SEMAPHORE *csim_init_semaphore(CSIM_SEMAPHORE *p_sem,
                                    CSIM_UWORD cnt);


/*
 * Locks the given semaphore for the current process. If no lock is available,
 * the process sleeps til it may continue.
 */
#define csim_lock_sem(p_sem)                                                  \
do {                                                                          \
  CSIM_SEMAPHORE *_p_sem;                                                     \
                                                                              \
  _p_sem = (p_sem);                                                           \
  if (_p_sem->count > 0)                                                      \
    (_p_sem->count)--;                                                        \
  else {                                                                      \
    csim_wait((CSIM_HEAD *) _p_sem);                                          \
  }                                                                           \
} while (0)


/*
 * Unlocks the given semaphore. Any locked semaphore must be freed after the
 * protected resource is no longer needed.
 */
void csim_unlock_sem(CSIM_SEMAPHORE *p_sem);

#endif
