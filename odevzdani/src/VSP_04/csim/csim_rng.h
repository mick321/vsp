/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_rng.c

                   header file of random number generator
                        Random number generator TT800
              by M. Matsumoto, email: matumoto@math.keio.ac.jp
                              1996 Version

*****************************************************************************/

#ifndef CSIM_RNG_H
#define CSIM_RNG_H 1

#include "csim.h"

#define CSIM_RAND_MAX 0xFFFFFFFFL         /* maximum value returned by rng */
#define CSIM_RNG_N 25

/*
 * The random generator state is stored within the following structure.
 * Usage example:
 * {
 *   RNG_STATE *rng;
 *   double x;
 *
 *   rng = s_new_rng(INIT_SEED);
 *     ...
 *   x = (double) csim_rand(rng) / (double) CSIM_RAND_MAX;
 *     ...
 *   dispose_rng(rng);
 * }
 */

/* Holds the current state of a random number generator (RNG). */
csim_derived_dyn_mem(csim_rng_state)
  CSIM_ULONG rng_array[CSIM_RNG_N];
  int index;
csim_end_derived(CSIM_RNG_STATE);

/*
 * Allocates dynamic memory for a new RNG and initializes the structure.
 * Returns pointer to the new dynamic memory.
 */
#define csim_new_rng(seed)                                                    \
  (CSIM_RNG_STATE *) csim_init_rng(                                           \
    (CSIM_RNG_STATE *) malloc(sizeof(CSIM_RNG_STATE)), seed);

/* Frees all resources asociated with specified RNG. */
#define csim_dispose_rng(rng)                                                 \
  csim_dispose_dyn_mem((CSIM_DYN_MEM *) rng);

/*
 * Initializes the specified RNG structure. Must be called explicitly if
 * staticaly allocated RNG is used.
 */
CSIM_RNG_STATE *csim_init_rng(CSIM_RNG_STATE *p_rng,
                              CSIM_ULONG seed);

/* Generates a single random number in the range <0,CSIM_RAND_MAX> */
CSIM_ULONG csim_rand(CSIM_RNG_STATE *p_rng);

/* Generator of exponential distribution */
double csim_negexp(CSIM_RNG_STATE *p_rng,
                   double lambda);

/* Generator of the regular distribution on the interval <a,b> */
double csim_uniform(CSIM_RNG_STATE *p_rng,
                    double a,
                    double b);

/* Returns TRUE with probability p (FALSE with probability 1-p) */
CSIM_BOOLEAN csim_draw(CSIM_RNG_STATE *p_rng,
                       double p);

/* Generator of the normal (Gaussian) distribution */
double csim_gauss(CSIM_RNG_STATE *p_rng,
                  double sigma,
                  double center);

#endif
