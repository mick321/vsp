/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_tm.h

                    C-Sim internal time representation

*****************************************************************************/

#ifndef CSIM_TM_H
#define CSIM_TM_H 1

typedef double CSIM_TIME;

#define CSIM_ZERO_TIME 0.0
#define csim_zero_time() (0.0)

/* basic arithmetic operation */
#define csim_time_add(op1, op2) ((op1) + (op2))
#define csim_time_sub(op1, op2) ((op1) - (op2))
#define csim_time_mul(op1, op2) ((op1) * (op2))
#define csim_time_div(op1, op2) ((op1) / (op2))
#define csim_time_cmp(op1, op2) ((op1) > (op2) ? 1 : (op1) < (op2) ? -1 : 0)

#define csim_time_to_dbl(time) ((double) time)
#define dbl_to_csim_time(time) ((CSIM_TIME) time)

#endif
