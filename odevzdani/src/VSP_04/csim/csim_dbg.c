/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_dbg.c

                         C-Sim console debug rutines

*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "csim_dbg.h"

GLOBAL csim_begin_view(CSIM_DYN_MEM, csim_view_dyn_mem)

  if (my.name != NULL)
    (void) printf("name : %s\n", my.name);
  else
    (void) printf("name : %s\n", "UNDEFINED");

  switch (my.type) {
    case 'M' :
      printf("type : CSIM_DYN_MEM\n");
      break;
    case 'L' :
      printf("type : CSIM_LINK\n");
      break;
    case 'H' :
      printf("type : CSIM_HEAD\n");
      break;
    case 'P' :
      printf("type : CSIM_PROCESS\n");
      break;
    default  : break;
  }

  if (my.destructor == NULL)
    (void) printf("destructor : NO\n");
  else
    (void) printf("destructor : YES\n");

  if (my.view == NULL)
   (void)  printf("view : NO\n");
  else {
    if ((my.view == csim_view_dyn_mem)
    || (my.view == csim_view_link)
    || (my.view == csim_view_head)
    || (my.view == csim_view_process))
      (void) printf("view : STANDARD\n");
    else
      (void) printf("view : USERS\n");
  }
csim_end_view


GLOBAL csim_begin_view(CSIM_LINK, csim_view_link)

  csim_view_dyn_mem(p_my);
  if (my.suc == NULL)
    (void) printf("suc : NULL\n");
  else {
    if (my.suc->name != NULL)
      (void) printf("suc : %p - (%s)\n", my.suc, my.suc->name);
    else
      (void) printf("suc : %p\n", my.suc);
  }

  if (my.pred == NULL)
    (void) printf("pred : NULL\n");
  else {
    if (my.pred->name != NULL)
      (void) printf("pred : %p - (%s)\n", my.pred, my.pred->name);
    else
      (void) printf("pred : %p\n", my.pred);
  }

  if (my.head == NULL)
    (void) printf("head : NULL\n");
  else {
    if (my.head->name != NULL)
      (void) printf("head : %p - (%s)\n", my.head, my.head->name);
    else
      (void) printf("head : %p\n",my.head);
  }

csim_end_view


GLOBAL csim_begin_view(CSIM_HEAD, csim_view_head)

  csim_view_link(p_my);
  if (my.p_first == NULL)
    (void) printf("p_first : NULL\n");
  else {
    if (my.p_first->name != NULL)
      (void) printf("p_first : %p - (%s)\n", my.p_first, my.p_first->name);
    else
      (void) printf("p_first : %p\n", my.p_first);
  }

  if (my.p_last == NULL)
    (void) printf("p_last : NULL\n");
  else {
    if (my.p_last->name != NULL)
      (void) printf("p_last : %p - (%s)\n", my.p_last, my.p_last->name);
    else
      (void) printf("p_last : %p \n", my.p_last);
  }

  (void) printf("lq : %u\n", my.lq);

  if (my.statistics == TRUE)
    (void) printf("statistics : TRUE\n");
  else
    (void) printf("statistics : FALSE\n");

csim_end_view


GLOBAL csim_begin_view(CSIM_PROCESS, csim_view_process)

  csim_view_link(p_my);
  if (my.suc_in_sqs == NULL)
    (void) printf("suc_in_sqs : NULL\n");
  else {
    if (my.suc_in_sqs != NULL)
      (void) printf("suc_in_sqs : %p - (%s)\n",
        my.suc_in_sqs, my.suc_in_sqs->name);
    else
      (void) printf("suc_in_sqs : %p\n", my.suc_in_sqs);
  }

  if (my.pred_in_sqs == NULL)
    (void) printf("pred_in_sqs : NULL\n");
  else {
    if (my.pred_in_sqs != NULL)
      (void) printf("pred_in_sqs : %p - (%s)\n",
        my.pred_in_sqs, my.pred_in_sqs->name);
    else
      (void) printf("pred_in_sqs : %p\n", my.pred_in_sqs);
  }

  switch (my.state) {
    case CSIM_NEW_PASSIVE :
      printf("state : CSIM_NEW_PASSIVE\n");
      break;
    case CSIM_NEW_PLANNED :
      printf("state : CSIM_NEW_PLANNED\n");
      break;
    case CSIM_ACTIVE :
      printf("state : CSIM_ACTIVE\n");
      break;
    case CSIM_PLANNED :
      printf("state : CSIM_PLANNED\n");
      break;
    case CSIM_PASSIVE :
      printf("state : CSIM_PASSIVE\n");
      break;
    case CSIM_TERMINATED :
      printf("state : CSIM_TERMINATED\n");
      break;
    default : break;
  }

  (void) printf("evtime : %f\n", csim_time_to_dbl(my.evtime));

  if (my.statistics == TRUE)
    (void) printf("statistics : TRUE\n");
  else
    (void) printf("statistics : FALSE\n");

csim_end_view


GLOBAL void csim_debug(CSIM_DYN_MEM *p_dyn) {
  CSIM_DYN_MEM *p_dyn_mem;
  CSIM_BOOLEAN exit_debug = FALSE;
  CSIM_BOOLEAN repeat = FALSE;
  int c;

  if (p_dyn != NULL)
    p_dyn_mem = p_dyn;
  else
    p_dyn_mem = csim_mem_point()->p_suc_mem;
  do {
    if (repeat != TRUE) {
      p_dyn_mem->view(p_dyn_mem);
      (void) printf("ENTER: 1 - successor, 2 - predecessor, 3 - csim_first,"
       " 4 - csim_last, Q - quit\n");
    }
    c = getchar();
    while (getchar() != '\n')
      ;
    repeat = FALSE;
    switch (c) {
      case '1' :
        p_dyn_mem = p_dyn_mem->p_suc_mem;
        break;

      case '2' :
        p_dyn_mem = p_dyn_mem->p_pred_mem;
        break;

      case '3' :
        p_dyn_mem = csim_mem_point()->p_suc_mem;
        break;

      case '4' :
        p_dyn_mem = csim_mem_point()->p_pred_mem;
        break;

      case 'q' :
      case 'Q' :
        exit_debug = TRUE;
        break;

      default :
        repeat = TRUE;
        break;
    }
  } while(exit_debug != TRUE);
}

