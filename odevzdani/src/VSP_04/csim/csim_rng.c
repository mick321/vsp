/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_rng.c

                      random number generator functions

*****************************************************************************/

#include <math.h>
#include "csim_rng.h"

#define INIT_SEED ((unsigned) 4367)       /* init value of random gen., */
                                          /* must be NONZERO            */
#define CSIM_RNG_M 7

CSIM_RNG_STATE *csim_init_rng(CSIM_RNG_STATE *p_rng,
                              CSIM_ULONG seed)
{
  if (p_rng != NULL) {
    csim_init_dyn_mem((CSIM_DYN_MEM *) p_rng);

    if (seed == 0)
      p_rng->rng_array[0] = INIT_SEED;
    else
      p_rng->rng_array[0] = seed;

    for (p_rng->index = 1; p_rng->index < CSIM_RNG_N; p_rng->index++)
      p_rng->rng_array[p_rng->index] =
        69069L * p_rng->rng_array[p_rng->index - 1];

  }

  return p_rng;
}


CSIM_ULONG csim_rand(CSIM_RNG_STATE *p_rng)
{
  CSIM_ULONG y;
  static CSIM_ULONG mag01[2] = { 0x0, 0x8EBFD028L};

  if (p_rng->index == CSIM_RNG_N) {
    int k;
    for (k=0; k < CSIM_RNG_N - CSIM_RNG_M; k++) {
      p_rng->rng_array[k] = p_rng->rng_array[k + CSIM_RNG_M]
        ^ (p_rng->rng_array[k] >> 1) ^ mag01[p_rng->rng_array[k] % 2];
    }
    for (; k < CSIM_RNG_N; k++) {
      p_rng->rng_array[k] = p_rng->rng_array[k + (CSIM_RNG_M - CSIM_RNG_N)]
        ^ (p_rng->rng_array[k] >> 1) ^ mag01[p_rng->rng_array[k] % 2];
    }
    p_rng->index = 0;
  }
  y = p_rng->rng_array[p_rng->index];
  y ^= (y << 7)  & 0x2B5B2500L;
  y ^= (y << 15) & 0xDB8B0000L;
  y ^= (y >> 16);
  p_rng->index++;

  return y;
}

double csim_negexp(CSIM_RNG_STATE *p_rng,
                   double lambda)
{
  double tmp;

  do {
    tmp = (double) csim_rand(p_rng) / ((double) CSIM_RAND_MAX);
  } while (tmp == 0.0);
  return -(log(tmp) / lambda);
}


double csim_uniform(CSIM_RNG_STATE *p_rng,
                    double a,
                    double b)
{
  return (a + ((double) csim_rand(p_rng) / (double) CSIM_RAND_MAX) * (b - a));
}


CSIM_BOOLEAN csim_draw(CSIM_RNG_STATE *p_rng,
                       double p)
{
  if (((double) csim_rand(p_rng) / (double) CSIM_RAND_MAX) < p)
    return (TRUE);
  else
    return (FALSE);
}


double csim_gauss(CSIM_RNG_STATE *p_rng,
                  double sigma,
                  double stred)
{
  int j;
  double f, sum;

  sum = 0.0;
  for (j = 1; j <= 12; j++) {
    f = csim_rand(p_rng) / (double) CSIM_RAND_MAX;
    sum += f;
  }
  return (sigma * (sum - 6.0) + stred);
}
