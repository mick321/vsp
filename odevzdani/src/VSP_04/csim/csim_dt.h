/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_dt.h

                         C-Sim library data types

*****************************************************************************/

#ifndef CSIM_DT_H
#define CSIM_DT_H 1

#include <limits.h>
#include <float.h>

/* declaration symbols */
#define GLOBAL          /**/      /* global object      */
#define IMPORT          extern    /* extern declaration */
#define EXPORT          extern
#define LOCAL           static    /* local object */
#define REG             register  /* abbreviation */

typedef enum {
  SUCCESS,                        /* true  */
  FAILURE                         /* false */
} CSIM_RESULT;                    /* boolean function result */

#undef FALSE
#undef TRUE

typedef enum {
  FALSE,
  TRUE
} CSIM_BOOLEAN;                 /* boolean type */

typedef signed   char      CSIM_BYTE;        /* 8-bit number with sign     */
typedef unsigned char      CSIM_UBYTE;       /* 8-bit number without sign  */

#if UINT_MAX >= 4294967295L
typedef signed   short int CSIM_WORD;        /* 16-bit number with sign    */
typedef unsigned short int CSIM_UWORD;       /* 16-bit number without sign */
typedef signed         int CSIM_LONG;        /* 32-bit number with sign    */
typedef unsigned       int CSIM_ULONG;       /* 32-bit number without sign */
#else
typedef signed         int CSIM_WORD;        /* 16-bit number with sign    */
typedef unsigned       int CSIM_UWORD;       /* 16-bit number without sign */
typedef signed   long  int CSIM_LONG;        /* 32-bit number with sign    */
typedef unsigned long  int CSIM_ULONG;       /* 32-bit number without sign */
#endif

#endif
