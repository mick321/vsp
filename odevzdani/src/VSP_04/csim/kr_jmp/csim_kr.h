/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                03.04.2003

                             C-Sim version 5.1

                              file csim_kr.h

             header file of C-Sim kernel based on setjmp/longjmp

*****************************************************************************/

#ifndef CSIM_KR_H
#define CSIM_KR_H 1

#include <setjmp.h>

#define csim_process_kr                                                       \
  jmp_buf rollback


#define csim_create_process(process)                                          \
  if (setjmp(csim_sqs_point()->rollback) == 0) {                              \
    process->program(process);                                                \
  }

#define csim_switch_processes(from, to)                                       \
  if (setjmp(from->rollback) == 0) {                                          \
    longjmp(to->rollback, 1);                                                 \
  }

#define csim_return_to_step()                                                 \
  longjmp(csim_sqs_point()->rollback,1)

#define csim_cancel_process(process)

#define csim_init_kr(process)

#define csim_restore_process                                                  \
  do {                                                                        \
    void **_csim_tmp;                                                         \
    _csim_tmp = (void **) &p_my;                                              \
    *_csim_tmp = (void *) csim_current();                                     \
  } while (0)

#endif
