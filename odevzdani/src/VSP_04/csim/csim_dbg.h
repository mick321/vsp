/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_dgb.h

                 header file of C-Sim console debug rutines

*****************************************************************************/

#ifndef CSIM_DBG_H
#define CSIM_DBG_H 1

#include "csim.h"

/* 
 * Basic C-Sim object VIEW functions for console oriented output. Every
 * object uses different function - specified in the 'view' attribute. 
 */    

/* Shows the CSIM_DYN_MEM object */
void csim_view_dyn_mem(void *p_void);

/* Shows the CSIM_LINK object */
void csim_view_link(void *p_void);

/* Shows the CSIM_HEAD object */
void csim_view_head(void *p_void);

/* Shows the CSIM_PROCESS object */
void csim_view_process(void *p_void);

/* Function for viewing the list of dynamically generated objects */
void csim_debug(CSIM_DYN_MEM *p_dyn);

#endif
