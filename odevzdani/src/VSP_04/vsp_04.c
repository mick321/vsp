#include <stdio.h>
#include <time.h>
#include "csim/csim.h"
#include "csim/csim_err.h"
#include "csim/csim_sem.h"
#include "csim/csim_rng.h"

const int debugPrints = 0;
int MAX_MESSAGES = 250000;

/* prototypy */
typedef struct CONSUMER;
typedef struct PRODUCER;

/* zprava*/
typedef struct {
  csim_d_link;
  double creationTime; /* cas vytvoreni zpravy*/
} TRANSACTION;

/* definice bufferu (fronty zprav)*/
typedef struct{
  csim_d_head;
  CSIM_SEMAPHORE* semaphoreEmpty; /* semafor - prazdno*/
  CSIM_SEMAPHORE* semaphoreFull; /* semafor - plno*/
  int maxQueueLength; /* maximalni delka fronty*/
} BUFFER;

/* definice producenta zprav*/
typedef struct{
  csim_d_process; /* derived from CSIM_PROCESS */
  BUFFER* queue; /* odkaz na navazujici frontu zprav*/
  float lambda; /* parametr exponencialniho rozdeleni*/
} PRODUCER;

/* definice prijemce zprav*/
typedef struct{
  csim_d_process; /* derived from CSIM_PROCESS */
  BUFFER* queue; /* odkaz na frontu zprav*/
  float mu; /* parametr exponencialniho rozdeleni*/

  int totalMessages; /* celkovy pocet dorucenych zprav */
  double totalWaitTime; /* celkovy cas cekani */
  double tempLastTime; /* docasne uloziste casu pri zahajeni cekani */
} CONSUMER;

/* global random number generator*/
CSIM_RNG_STATE* random;
/* buffer (fronta)*/
BUFFER* buffer;
/* producent*/
PRODUCER* producer;
/* konzument (prijemce)*/
CONSUMER* consumer;

/* ------------------------------------------------------------------------*/
/* program producenta*/
csim_program(PRODUCER, PRODUCER_PROG)
  for (;;) {
    
    if (debugPrints)
      printf("P: Naplanovan producent\n");

    if (csim_cardinal((CSIM_HEAD *)my.queue) == my.queue->maxQueueLength)
    {
      if (debugPrints)
        printf("P: Ceka na misto ve fronte...\n");
      csim_lock_sem(my.queue->semaphoreFull); /* cekam, dokud se neuvolni misto*/
      if (debugPrints)
        printf("P: Konec cekani\n");
    }
    
    if (debugPrints)
      printf("P: Fronta ma %d polozek\n", (int)csim_cardinal((CSIM_HEAD *)my.queue));
    {
      TRANSACTION *p_poin; /* auxiliary pointer */
      p_poin = csim_new_link(TRANSACTION);
      p_poin->creationTime = csim_time();
      if (debugPrints)
        printf("P: Vklada polozku do fronty\n");
      csim_into((CSIM_LINK *)p_poin, (CSIM_HEAD *)my.queue); /* vlozeni polozky*/
    }
    if (debugPrints)
      printf("P: Fronta ma %d polozek\n", (int)csim_cardinal((CSIM_HEAD *)my.queue));

    {
      if (debugPrints)
        printf("P: Notifikace konzumentovi\n");

      csim_unlock_sem(my.queue->semaphoreEmpty); /* notifikace, ze je v seznamu nova polozka*/
      my.queue->semaphoreEmpty->count = 0; /* vzhledem k tomu, ze c-sim nedodrzuje maximum nastavene u semaforu, dodrzim ho ja za nej */
    }

    if (debugPrints)
      printf("P: Naplanovani s exp. rozdelenim\n");
    csim_hold(csim_negexp(random, my.lambda)); /* naplanovani*/
  }
csim_end_process

/* ------------------------------------------------------------------------*/
/* program konzumenta (prijemce)*/
csim_program(CONSUMER, CONSUMER_PROG)
  for (;;) {

    my.totalMessages++;

    if (debugPrints)
      printf("K: Naplanovan konzument\n");

    /* vyjmuti polozky*/
    if (csim_cardinal((CSIM_HEAD *)my.queue) == 0)
    {
      if (debugPrints)
        printf("K: Ceka na polozku...\n");
      my.tempLastTime = csim_time();
      csim_lock_sem(my.queue->semaphoreEmpty); /* cekame na polozku*/
      my.totalWaitTime += (csim_time() - my.tempLastTime);
      if (debugPrints)
        printf("K: Konec cekani\n");
    }

    if (debugPrints)
      printf("K: Fronta ma %d polozek\n", (int)csim_cardinal((CSIM_HEAD *)my.queue));
    {
      TRANSACTION *p_poin; /* auxiliary pointer */
      if (debugPrints)
        printf("K: Vybira polozku\n");
      p_poin = (TRANSACTION *)csim_first((CSIM_HEAD *)my.queue);
      if (p_poin)
        csim_dispose_link((CSIM_LINK *)p_poin);
    }
    if (debugPrints)
      printf("K: Fronta ma %d polozek\n", (int)csim_cardinal((CSIM_HEAD *)my.queue));

    {
      if (debugPrints)
        printf("K: Notifikace producentovi\n");
      csim_unlock_sem(my.queue->semaphoreFull); /* uvolneno misto*/
      my.queue->semaphoreFull->count = 0; /* vzhledem k tomu, ze c-sim nedodrzuje maximum nastavene u semaforu, dodrzim ho ja za nej */
    }

    if (debugPrints)
      printf("K: Naplanovani s exp. rozdelenim\n");
    csim_hold(csim_negexp(random, my.mu)); /* naplanovani*/
  }
csim_end_process

/* ------------------------------------------------------------------------*/
/* zakladni inicializace programu*/
int init(float lambda, float mu, int maxQueueLength)
{
  if (csim_init_mem() != SUCCESS)
  {
    return 1;
  }

  /* generator nahodnych cisel*/
  random = csim_new_rng((int)time(NULL));
  if (random == NULL)
  {
    return 1;
  }
  
  /* alokuj objekty*/
  buffer = csim_new_head(BUFFER);
  if (buffer == NULL)
    return 1;
  producer = csim_new_process(PRODUCER, PRODUCER_PROG); 
  if (producer == NULL)
    return 1;
  consumer = csim_new_process(CONSUMER, CONSUMER_PROG);
  if (consumer == NULL)
    return 1;

  /* inicializuj parametry*/
  if (lambda <= 0.f)
    lambda = 2.f;
  if (mu <= 0.f)
    mu = 2.f;
  if (maxQueueLength < 1)
    maxQueueLength = 1;

  producer->queue = buffer;
  producer->lambda = lambda;
  
  consumer->queue = buffer;
  consumer->mu = mu;
  consumer->totalMessages = -1; /* zprava se zpracovava na zacatku programu konzumenta - prvni beh vsak nema k dispozici, proto -1 */
  consumer->totalWaitTime = 0;

  buffer->semaphoreEmpty = csim_new_semaphore(0);
  buffer->semaphoreFull = csim_new_semaphore(0);
  buffer->maxQueueLength = maxQueueLength;

  return 0;
}

/* ------------------------------------------------------------------------*/
/* deinicializace programu*/
int deinit()
{
  if (buffer->semaphoreEmpty)
  {
    csim_dispose_semaphore(buffer->semaphoreEmpty);
    buffer->semaphoreEmpty = NULL;
  }
  if (buffer->semaphoreFull)
  {
    csim_dispose_semaphore(buffer->semaphoreFull);
    buffer->semaphoreFull = NULL;
  }

  if (producer)
  {
    csim_dispose_process((CSIM_PROCESS *)producer);
    producer = NULL;
  }
  if (consumer)
  {
    csim_dispose_process((CSIM_PROCESS *)consumer);
    consumer = NULL;
  }
  if (buffer)
  {
    csim_dispose_head((CSIM_HEAD *)buffer);
    buffer = NULL;
  }
  
  if (random)
  {
    csim_dispose_rng(random);
    random = NULL;
  }

  csim_clear_mem();
  return 0;
}

/* ------------------------------------------------------------------------*/
/* vstupni bod programu*/
int main(int argc, char* argv[])
{
  int nextprint;
  float lambda = 2.f, mu = 2.f;
  int bufferlen = 1;

  /* vytisknuti napovedy */
  if (argc == 1)
  {
    printf("Parametry programu:\nVSP_04.exe [pocet pozadavku] [lambda] [mu] [delka fronty]\n");
    printf("pocet pozadavku - celkovy pocet pozadavku simulace\nlambda - frekvence produkce zprav\n");
    printf("mu - frekvence zpracovani zprav\ndelka fronty urcuje maximalni mozny pocet zprav ve fronte\n\n");
  }

  /* prevzeti parametru z prikazove radky */
  if (argc >= 2)
    MAX_MESSAGES = strtol(argv[1], NULL, 10);
  if (argc >= 3)
    lambda = (float)strtod(argv[2], NULL);
  if (argc >= 4)
    mu = (float)strtod(argv[3], NULL);
  if (argc >= 5)
    bufferlen = strtol(argv[4], NULL, 10);

  /* inicializace */
  if (init(lambda, mu, bufferlen) != 0 || MAX_MESSAGES <= 0)
  {
    printf("Doslo k chybe pri inicializaci.\n");
    deinit();
    return 1;
  }

  /* rozbehni simulaci */

  printf("Simulace bezi s nasledujicimi parametry:\npocet pozadavku = %d\nlambda = %.2f\n"
    "mu = %.2f\ndelka fronty = %d\n\n", MAX_MESSAGES, lambda, mu, bufferlen);

  csim_activate_at((CSIM_PROCESS*)producer, (CSIM_TIME)0);
  csim_activate_at((CSIM_PROCESS*)consumer, (CSIM_TIME)0);

  nextprint = 0;
  while (consumer->totalMessages < MAX_MESSAGES)
  {
    if (consumer->totalMessages == nextprint)
    {
      nextprint += MAX_MESSAGES / 20;
      printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"); /* vymazani radku */
      printf("Hotovo %d%% simulace", 100 * consumer->totalMessages / MAX_MESSAGES);
    }
    if (csim_step() != SUCCESS)
    {
      printf("Behem simulace doslo k chybe.\n");
      break;
    }
  }

  printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b"); /* vymazani radku */
  printf("Hotovo %d%% simulace\n", 100 * consumer->totalMessages / MAX_MESSAGES);

  printf("\nVYSLEDKY (po %d pozadavcich):\n%d zprav doruceno a zpracovano\n%.4lf zprav za vterinu\n%.4lf procent casu konzument cekal nad prazdnou frontou\n",
    MAX_MESSAGES, consumer->totalMessages, consumer->totalMessages / (double)csim_time(), 100.0 * consumer->totalWaitTime / (double)csim_time());
  /*getchar();*/

  deinit();
  return 0;
}