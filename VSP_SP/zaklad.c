/* pomocne struktury pouzivane vsemi objekty */ 

#include "zaklad.h"
#include "zprava.h"
#include "fronta.h"
#include "krizovatka.h"
#include "zdroj_stok.h"

/* genericke predavani zpravy SHO */
void pokracuj(POKRACOVANI* pokr, ZPRAVA* zprava)
{
  assert(pokr);
  assert(zprava);
  assert(pokr->pointer);
  switch (pokr->typ)
  {
  case TYP_FRONTA:
    fronta_pokracuj((FRONTA*)pokr->pointer, (ZPRAVA*)zprava);
    break;
  case TYP_STOK:
    stok_pokracuj((STOK*)pokr->pointer, (ZPRAVA*)zprava);
    break;
  case TYP_KRIZOVATKA:
    krizovatka_pokracuj((KRIZOVATKA*)pokr->pointer, (ZPRAVA*)zprava);
    break;
  default:
    assert(0);
    break;
  }
}

/* nahodne cislo s uniformnim rozdelenim */
double random_uniform(double minimum, double maximum)
{
  int rnd = rand();
  return minimum + rnd * (maximum - minimum) / RAND_MAX; /* vicemene rovnomerne rozdeleni...*/
}

/* nahodne cislo s exponencialnim rozdelenim, zaporna hodnota */
double random_negexp(double lambda)
{
  double tmp;
  do
  {
    tmp = random_uniform(0, 1);
  } while (tmp <= 0.0 || tmp >= 1.0);
  return -(log(tmp) / lambda);
}

/* nahodne cislo s gaussovkym rozdelenim - vraci vsak pouze kladna cisla*/
double random_gauss_kladne(double mu, double odchylka)
{
  int i;
  double temp;
  do
  {
    temp = 0.0;
    for (i = 0; i < 12; i++)
      temp += random_uniform(0, 1);

    temp = (temp - 6) * odchylka + mu;
  } while (temp < 0);
  return temp;
}

/* --------------------------------------------------------------------- */

/* vytvor defaultni strukturu statistik */
void stats_default(STATS* stats)
{
  assert(stats);
  stats->pocet = 0;
  stats->suma = 0;
  stats->suma2 = 0;
}

/* zapocti hodnotu do statistiky */
void stats_zapocti(STATS* stats, double ppst, double hodnota)
{
  assert(stats);
  stats->pocet += ppst;
  stats->suma += ppst*hodnota;
  stats->suma2 += ppst*hodnota*hodnota;
}

/* stredni hodnota */
double stats_stredni_hodnota(STATS* stats)
{
  return stats->pocet ? (stats->suma / stats->pocet) : 0.0;
}

/* rozptyl */
double stats_rozptyl(STATS* stats)
{
  if (stats->pocet == 0)
    return 0;
  double ex = stats->suma / stats->pocet;
  double ex2 = stats->suma2 / stats->pocet;
  return ex2 - ex * ex;
}

/* smerodatna odchylka */
double stats_odchylka(STATS* stats)
{
  return sqrt(stats_rozptyl(stats));
}
