#pragma once

#include "zaklad.h"
#include "zprava.h"
#include "fronta.h"

/* struktura kanalu obsluhy */
typedef struct
{
  csim_d_process; /* odvozeni od typu process */
  FRONTA* fronta; /* pointer na pridruzenou frontu */
  POKRACOVANI pokracovani; /* genericke pokracovani, kam predavat pozadavky */

  ROZDELENI pouziteRozdeleni;
  double mu; /* stredni frekvence zpracovani pozadavku */
  double mu_odchylka; /* odchylka frekvence zpracovani pozadavku */
  
  double casPosledniAktivity; /* pomocna promenna, cas posledni aktivity */
  ZPRAVA* zpracovavanyPozadavek; /* pozadavek zpracovavany kanalem obsluhy */

  STATS stredniDobaZpracovani; /* statistika - stredni doba zpracovani pozadavku */
  STATS periodaPruchoduPoz; /* statistika - stredni perioda mezi jednotlivymi pozadavky */
  STATS dobaOdezvy; /* statistika - vypocet doby odezvy */

  double casAktivni; /* celkovy cas, kdy kanal obsluhy pracoval */
} KANAL_OBSLUHY;

/* vytvor kanal obsluhy s vazbou na danou frontu a parametrem rozdeleni zpracovani pozadavku */
KANAL_OBSLUHY* kanal_obsluhy_vytvor(FRONTA* fronta, ROZDELENI pouziteRozdeleni, double mu, double mu_odchylka);
/* uvolni kanal obsluhy */
void kanal_obsluhy_uvolni(KANAL_OBSLUHY* kanal);

