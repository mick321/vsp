#include <stdio.h>
#include <time.h>
#include "csim/csim.h"
#include "csim/csim_err.h"
#include "csim/csim_sem.h"
#include "csim/csim_rng.h"
#include "zaklad.h"
#include "fronta.h"
#include "kanal_obsluhy.h"
#include "krizovatka.h"
#include "zdroj_stok.h"

/* globalni promenne potrebne pro nase SHO */

const double mu1 = 20;
const double mu2 = 15;
const double mu3 = 10;
const double mu4 = 25;
const double lambda1 = 16;
const double lambda2 = 8;

KANAL_OBSLUHY* kanal[4];
FRONTA* fronta[4];
KRIZOVATKA* krizovatka[2];
ZDROJ* zdroj[2];
STOK* stok[1];

int pocetPozadavku = 1000000;
ROZDELENI pouziteRozdeleni = EXP;
double pouzityKoefVariace = 1.0; 
double casStartu;

/* ------------------------------------------------------------------------*/
/* zakladni inicializace programu*/
int init()
{
  srand((int)time(NULL));

  /* vytvoreni objektu */
  zdroj[0] = zdroj_vytvor(pouziteRozdeleni, lambda1, pouzityKoefVariace / lambda1);
  zdroj[1] = zdroj_vytvor(pouziteRozdeleni, lambda2, pouzityKoefVariace / lambda2);

  stok[0] = stok_vytvor();

  fronta[0] = fronta_vytvor();
  fronta[1] = fronta_vytvor();
  fronta[2] = fronta_vytvor();
  fronta[3] = fronta_vytvor();

  kanal[0] = kanal_obsluhy_vytvor(fronta[0], pouziteRozdeleni, mu1, pouzityKoefVariace / mu1);
  kanal[1] = kanal_obsluhy_vytvor(fronta[1], pouziteRozdeleni, mu2, pouzityKoefVariace / mu2);
  kanal[2] = kanal_obsluhy_vytvor(fronta[2], pouziteRozdeleni, mu3, pouzityKoefVariace / mu3);
  kanal[3] = kanal_obsluhy_vytvor(fronta[3], pouziteRozdeleni, mu4, pouzityKoefVariace / mu4);

  krizovatka[0] = krizovatka_vytvor();
  krizovatka[1] = krizovatka_vytvor();

  /* nastaveni vazeb */
  zdroj[0]->pokracovani.typ = TYP_FRONTA;
  zdroj[0]->pokracovani.pointer = fronta[0];
  zdroj[1]->pokracovani.typ = TYP_FRONTA;
  zdroj[1]->pokracovani.pointer = fronta[1];

  kanal[0]->pokracovani.typ = TYP_KRIZOVATKA;
  kanal[0]->pokracovani.pointer = krizovatka[0];
  kanal[1]->pokracovani.typ = TYP_KRIZOVATKA;
  kanal[1]->pokracovani.pointer = krizovatka[0];

  POKRACOVANI pokracovaniTemp;
  pokracovaniTemp.typ = TYP_FRONTA;
  pokracovaniTemp.pointer = fronta[2];
  krizovatka_pridej_moznost(krizovatka[0], pokracovaniTemp, 0.2);
  pokracovaniTemp.typ = TYP_FRONTA;
  pokracovaniTemp.pointer = fronta[3];
  krizovatka_pridej_moznost(krizovatka[0], pokracovaniTemp, 0.8);

  kanal[2]->pokracovani.typ = TYP_KRIZOVATKA;
  kanal[2]->pokracovani.pointer = krizovatka[1];
  kanal[3]->pokracovani.typ = TYP_KRIZOVATKA;
  kanal[3]->pokracovani.pointer = krizovatka[1];

  pokracovaniTemp.typ = TYP_FRONTA;
  pokracovaniTemp.pointer = fronta[0];
  krizovatka_pridej_moznost(krizovatka[1], pokracovaniTemp, 0.1);
  pokracovaniTemp.typ = TYP_FRONTA;
  pokracovaniTemp.pointer = fronta[1];
  krizovatka_pridej_moznost(krizovatka[1], pokracovaniTemp, 0.1);
  pokracovaniTemp.typ = TYP_STOK;
  pokracovaniTemp.pointer = stok[0];
  krizovatka_pridej_moznost(krizovatka[1], pokracovaniTemp, 0.8);

  /* naplanovani */
  csim_activate_at((CSIM_PROCESS*)zdroj[0], csim_time());
  csim_activate_at((CSIM_PROCESS*)zdroj[1], csim_time());
  csim_activate_at((CSIM_PROCESS*)kanal[0], csim_time());
  csim_activate_at((CSIM_PROCESS*)kanal[1], csim_time());
  csim_activate_at((CSIM_PROCESS*)kanal[2], csim_time());
  csim_activate_at((CSIM_PROCESS*)kanal[3], csim_time());

  casStartu = csim_time();
  return 0;
}

/* ------------------------------------------------------------------------*/
/* deinicializace programu*/
int deinit()
{
  kanal_obsluhy_uvolni(kanal[0]);
  kanal_obsluhy_uvolni(kanal[1]);
  kanal_obsluhy_uvolni(kanal[2]);
  kanal_obsluhy_uvolni(kanal[3]);
  fronta_uvolni(fronta[0]);
  fronta_uvolni(fronta[1]);
  fronta_uvolni(fronta[2]);
  fronta_uvolni(fronta[3]);
  krizovatka_uvolni(krizovatka[0]);
  krizovatka_uvolni(krizovatka[1]);
  zdroj_uvolni(zdroj[0]);
  zdroj_uvolni(zdroj[1]);
  stok_uvolni(stok[0]);
  return 0;
}

/* ------------------------------------------------------------------------*/
int ukaz_napovedu()
{
  printf("Program se spousti z prikazove radky:\n\n");
  printf("VSP_SP.exe [pocet pozadavku] [nahodne rozdeleni]\n");
  printf("[pocet pozadavku] - pocet simulovanych pozadavku (default: 100000)\n");
  printf("[nahodne rozdeleni] - dve mozne hodnoty - EXP / GAUSS (default: EXP)\n");
  printf("                    - nastavuje pouzite nahodne rozdeleni\n\n");
  return 0;
}

/* ------------------------------------------------------------------------*/
int prepsat_radek()
{
  printf("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
  return 0;
}

/* ------------------------------------------------------------------------*/
/* zpracovani vstupnich parametru programu */
int zpracuj_parametry(int argc, char* argv[])
{
  int i;
  pocetPozadavku = 1000000;
  pouziteRozdeleni = EXP;

  if (argc <= 1)
  {
    ukaz_napovedu();
    return 0;
  }

  /* testovani gaussovskeho nahodneho rozdeleni */ 
  if (!strcmp(argv[1], "test-gauss"))
  {
    printf("testovani gaussovskeho rozdeleni\n");
    STATS stats;
    stats_default(&stats);
    for (int i = 0; i < 100000; i++)
    {
      stats_zapocti(&stats, 1, random_gauss_kladne(10, 1));
    }
    printf("Pro teoreticke mu=10 a sigma=1 byly namereny mu=%lf sigma=%lf\n", stats_stredni_hodnota(&stats), stats_odchylka(&stats));
    stats_default(&stats);
    for (int i = 0; i < 100000; i++)
    {
      stats_zapocti(&stats, 1, random_gauss_kladne(30, 2));
    }
    printf("Pro teoreticke mu=30 a sigma=2 byly namereny mu=%lf sigma=%lf\n", stats_stredni_hodnota(&stats), stats_odchylka(&stats));
    stats_default(&stats);
    for (int i = 0; i < 100000; i++)
    {
      stats_zapocti(&stats, 1, random_gauss_kladne(10, 2));
    }
    printf("Pro teoreticke mu=10 a sigma=2 byly namereny mu=%lf sigma=%lf\n", stats_stredni_hodnota(&stats), stats_odchylka(&stats));
    return 1;
  }

  if (1 != sscanf(argv[1], "%d", &pocetPozadavku) || pocetPozadavku < 1)
  {
    ukaz_napovedu();
    printf("Prvni parametr programu musi byt kladne cele cislo.\n");
    return 1;
  }

  if (argc <= 2)
    return 0;

  char temp[128];
  int len = strlen(argv[2]);
  for (i = 0; i < len && i < 127; i++)
  {
    temp[i] = toupper(argv[2][i]);
    temp[i + 1] = '\0';
  }
  
  if (!strcmp(temp, "EXP"))
    pouziteRozdeleni = EXP;
  else if (!strcmp(temp, "GAUSS"))
    pouziteRozdeleni = GAUSS;
  else
  {
    ukaz_napovedu();
    printf("Druhy parametr programu musi byt EXP nebo GAUSS.\n (podle pozadovaneho rozdeleni produkce a zpracovani pozadavku)\n");
    return 1;
  }

  return 0;
}

/* ------------------------------------------------------------------------*/
/* samotny beh simulace */
int run()
{
  int nextprint = 0;

  while (stok[0]->casPozadavek.pocet < pocetPozadavku)
  {
    if (stok[0]->casPozadavek.pocet >= nextprint)
    {
      nextprint += pocetPozadavku / 100;
      prepsat_radek();
      printf("Hotovo %d%% simulace", (int)(100 * stok[0]->casPozadavek.pocet / pocetPozadavku));
    }

    if (csim_step() == FAILURE)
    {
      prepsat_radek();
      printf("                               \n");
      printf("Behem simulace doslo k chybe.\n"); 
      return 1;
    }
  }
  prepsat_radek();
  printf("                              \n");

  return 0;
}

/* ------------------------------------------------------------------------*/
/* vytisteni statistik */
int ukaz_statistiky()
{
  double ro[4];
  double L_q[4];

  for (int i = 0; i < 4; i++)
  {
    ro[i] = kanal[i]->casAktivni / (csim_time() - casStartu);
    L_q[i] = stats_stredni_hodnota(&fronta[i]->fronta_delka) + ro[i];
  }

  printf("Statistiky: Zpracovano %d pozadavku, rozdeleni: %s.\n", pocetPozadavku, pouziteRozdeleni == EXP ? "EXP" : "GAUSS");
  if (pouziteRozdeleni == GAUSS)
    printf("Simulovano s gaussovskym rozdelenim o variaci C=%.4lf\n", pouzityKoefVariace);
  printf("Pruchod pozadavku uzly\n(stredni frekvence=f, stredni perioda=T, sm. odchylka periody=T_o,\nstredni doba zpracovani=T_z, stredni frekvence zpracovani=f_z):\n"
    "doba odezvy uzlu=T_R\n"
    "1: f=%6.3lf, T=%6.3lf, T_o=%6.4lf, T_z=%6.3lf, f_z=%6.3lf, T_R=%6.3lf\n"
    "2: f=%6.3lf, T=%6.3lf, T_o=%6.4lf, T_z=%6.3lf, f_z=%6.3lf, T_R=%6.3lf\n"
    "3: f=%6.3lf, T=%6.3lf, T_o=%6.4lf, T_z=%6.3lf, f_z=%6.3lf, T_R=%6.3lf\n"
    "4: f=%6.3lf, T=%6.3lf, T_o=%6.4lf, T_z=%6.3lf, f_z=%6.3lf, T_R=%6.3lf\n",
    1.0 / stats_stredni_hodnota(&kanal[0]->periodaPruchoduPoz), stats_stredni_hodnota(&kanal[0]->periodaPruchoduPoz), stats_odchylka(&kanal[0]->periodaPruchoduPoz),
    stats_stredni_hodnota(&kanal[0]->stredniDobaZpracovani), 1.0 / stats_stredni_hodnota(&kanal[0]->stredniDobaZpracovani), stats_stredni_hodnota(&kanal[0]->dobaOdezvy),
    1.0 / stats_stredni_hodnota(&kanal[1]->periodaPruchoduPoz), stats_stredni_hodnota(&kanal[1]->periodaPruchoduPoz), stats_odchylka(&kanal[1]->periodaPruchoduPoz),
    stats_stredni_hodnota(&kanal[1]->stredniDobaZpracovani), 1.0 / stats_stredni_hodnota(&kanal[1]->stredniDobaZpracovani), stats_stredni_hodnota(&kanal[1]->dobaOdezvy),
    1.0 / stats_stredni_hodnota(&kanal[2]->periodaPruchoduPoz), stats_stredni_hodnota(&kanal[2]->periodaPruchoduPoz), stats_odchylka(&kanal[2]->periodaPruchoduPoz),
    stats_stredni_hodnota(&kanal[2]->stredniDobaZpracovani), 1.0 / stats_stredni_hodnota(&kanal[2]->stredniDobaZpracovani), stats_stredni_hodnota(&kanal[2]->dobaOdezvy),
    1.0 / stats_stredni_hodnota(&kanal[3]->periodaPruchoduPoz), stats_stredni_hodnota(&kanal[3]->periodaPruchoduPoz), stats_odchylka(&kanal[3]->periodaPruchoduPoz),
    stats_stredni_hodnota(&kanal[3]->stredniDobaZpracovani), 1.0 / stats_stredni_hodnota(&kanal[3]->stredniDobaZpracovani), stats_stredni_hodnota(&kanal[3]->dobaOdezvy));

  printf("Zatizeni uzlu (ro):\n"
    "1: ro=%5.3lf\n2: ro=%5.3lf\n3: ro=%5.3lf\n4: ro=%5.3lf\n",
    ro[0], ro[1], ro[2], ro[3]);

  printf("Delka front (stredni hodnota=L, sm. odchylka=L_o, pocet pozadavku v uzlu=L_q):\n");
  printf("1: L=%6.2lf L_o=%6.2lf, L_q=%6.2lf\n"
    "2: L=%6.2lf L_o=%6.2lf, L_q=%6.2lf\n"
    "3: L=%6.2lf L_o=%6.2lf, L_q=%6.2lf\n"
    "4: L=%6.2lf L_o=%6.2lf, L_q=%6.2lf\n",
    stats_stredni_hodnota(&fronta[0]->fronta_delka), stats_odchylka(&fronta[0]->fronta_delka), L_q[0],
    stats_stredni_hodnota(&fronta[1]->fronta_delka), stats_odchylka(&fronta[1]->fronta_delka), L_q[1],
    stats_stredni_hodnota(&fronta[2]->fronta_delka), stats_odchylka(&fronta[2]->fronta_delka), L_q[2],
    stats_stredni_hodnota(&fronta[3]->fronta_delka), stats_odchylka(&fronta[3]->fronta_delka), L_q[3]);
  printf("Pruchod pozadavku systemem (stredni doba pruchodu=t, sm. odchylka=t_o):\n"
    "t = %6.2lf t_o = %6.2lf\n",
    stats_stredni_hodnota(&stok[0]->casPozadavek), stats_odchylka(&stok[0]->casPozadavek));
  printf("Stredni pocet pozadavku v SHO=l_Q:\nl_Q=%6.2lf\n",
    L_q[0] + L_q[1] + L_q[2] + L_q[3]);

  return 0;
}

const double koeficientyVariace[] = { 0.01, 0.25, 0.5 };

/* ------------------------------------------------------------------------*/
/* vstupni bod programu*/
int main(int argc, char* argv[])
{
  int pocetSimulaci;
  if (zpracuj_parametry(argc, argv) != 0)
    return 1;

  /* zakladni inicializace */
  if (csim_init_mem() != SUCCESS)
  {
    return 1;
  }

  pocetSimulaci = (pouziteRozdeleni == GAUSS) ? 3 : 1;
  do
  {
    pouzityKoefVariace = koeficientyVariace[3 - pocetSimulaci];
    if (init() != 0)
    {
      printf("Doslo k chybe pri inicializaci.\n");
      deinit();
      return 1;
    }

    /* rozbehni simulaci */
    if (run() != 0)
    {
      deinit();
      return 1;
    }

    ukaz_statistiky();
    /*getchar();*/

    deinit();
  } while (--pocetSimulaci > 0);

  csim_clear_mem();
  return 0;
}