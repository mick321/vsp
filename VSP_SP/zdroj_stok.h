#pragma once

#include "zaklad.h"
#include "zprava.h"

/* zdroj pozadavku v systemu */
typedef struct
{
  csim_d_process; /* odvozeno od CSIM_PROCESS */
  POKRACOVANI pokracovani; /* genericky pointer na navazujici prvek */

  ROZDELENI pouziteRozdeleni;
  double lambda; /* frekvence generovani zprav*/
  double lambda_odchylka; /* odchylka frekvence generovani zprav*/
} ZDROJ;

/* stok pozadavku v systemu (vystup ze systemu) */
typedef struct
{
  STATS casPozadavek; /* doba pozadavku v systemu */
  STATS periodaPruchoduPoz; /* perioda pozadavku */
  double casPosledniAktivity;
} STOK;

/* vytvoreni zdroje */
ZDROJ* zdroj_vytvor(ROZDELENI pouziteRozdeleni, double lambda, double lambda_odchylka);
/* uvolneni zdroje */
void zdroj_uvolni(ZDROJ* zdroj);

/* vytvoreni stoku */
STOK* stok_vytvor();
/* uvolneni stoku */
void stok_uvolni(STOK* stok);

/* predani zpravy stoku */
int stok_pokracuj(STOK* stok, ZPRAVA* zprava);