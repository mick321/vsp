/*
  "Rozcesti", zprava muze putovat dale ruznymi smery podle urcenych ppsti - struktury a pridruzene funkce.
*/
#pragma once

#include "zaklad.h"
#include "zprava.h"

/* pomocna struktura, nepouzivat primo */
typedef struct
{
  csim_d_link; /* macro - odvozeni od typu "link" */
  POKRACOVANI pokracovani; /* nasledujici objekt */
  double ppst; /* pravdepodobnost pokracovani v tomto smeru */
} KRIZOVATKA_MOZNOST;

/* definice krizovatky*/
typedef struct{
  csim_d_head; /* macro - odvozeni od typu "head" */
} KRIZOVATKA;


/* vytvor krizovatku */
KRIZOVATKA* krizovatka_vytvor();
/* uvolni krizovatku */
void krizovatka_uvolni(KRIZOVATKA* krizovatka);

/* pridani vystupu krizovatce 
  krizovatka - instance krizovatky
  pokracovani - genericky pointer na navazujici strukturu
  ppst - pravdepodobnost, ze se zprava vyda timto spojem
*/
void krizovatka_pridej_moznost(KRIZOVATKA* krizovatka, POKRACOVANI pokracovani, double ppst);

/* predani zpravy krizovatce */
void krizovatka_pokracuj(KRIZOVATKA* pokr, ZPRAVA* zprava);