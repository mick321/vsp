/* pomocne struktury pouzivane vsemi objekty */ 
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "csim/csim.h"
#include "csim/csim_err.h"
#include "csim/csim_sem.h"
#include "csim/csim_rng.h"

/* ----------------------------------------------- */

/* ruzne typy pointeru (v C neni dedicnost) */
typedef enum
{
  TYP_FRONTA,
  TYP_STOK,
  TYP_KRIZOVATKA
} POKR_TYP;

/* odkaz na navazujici objekt v SHO */
typedef struct
{
  void* pointer; 
  POKR_TYP typ;
} POKRACOVANI;

/* ----------------------------------------------- */

#define assert_pokracovani(P) assert((P).pointer && (P).typ <= TYP_KRIZOVATKA)

/* genericke predavani zpravy SHO */
void pokracuj(POKRACOVANI* pokr, void* zprava);

/* ----------------------------------------------- */

/* nahodne cislo s uniformnim rozdelenim */
double random_uniform(double minimum, double maximum);
/* nahodne cislo s exponencialnim rozdelenim, zaporna hodnota */
double random_negexp(double lambda);
/* nahodne cislo s gaussovkym rozdelenim - vraci vsak pouze kladna cisla*/
double random_gauss_kladne(double mu, double odchylka);

/* ----------------------------------------------- */

/* struktura pro statistiku */
typedef struct
{
  double pocet; /* pocet zaznamu nebo soucet pravdepodobnosti */
  double suma;  /* suma zaznamu */
  double suma2; /* suma ctvercu */
} STATS;

/* vytvor defaultni strukturu statistik */
void stats_default(STATS* stats);
/* zapocti hodnotu (s danou ppsti) do statistiky */
void stats_zapocti(STATS* stats, double ppst, double hodnota);
/* stredni hodnota */
double stats_stredni_hodnota(STATS* stats);
/* rozptyl */
double stats_rozptyl(STATS* stats);
/* smerodatna odchylka */
double stats_odchylka(STATS* stats);


/* ----------------------------------------------- */

/* enumerace moznych rozdeleni */
typedef enum
{
  EXP,
  GAUSS
} ROZDELENI;
