#include "kanal_obsluhy.h"

/* program zdroje pozadavku*/
csim_program(KANAL_OBSLUHY, KANAL_OBSLUHY_PROG)
for (;;) 
{
  if (my.zpracovavanyPozadavek)
  {
    stats_zapocti(&my.dobaOdezvy, 1, csim_time() - my.zpracovavanyPozadavek->casZarazeniDoFronty);
    pokracuj(&my.pokracovani, my.zpracovavanyPozadavek);
    my.zpracovavanyPozadavek = NULL;
  }

  stats_zapocti(&my.stredniDobaZpracovani, 1, csim_time() - my.casPosledniAktivity); /* doba zpracovani pozadavku */
  my.casAktivni += csim_time() - my.casPosledniAktivity;

  csim_lock_sem(my.fronta->semaforFronta); /* cekani na polozku ve fronte */

  fronta_statistiky(my.fronta); /* zapocteni statistik fronty */

  {
    ZPRAVA* zprava = (ZPRAVA*)csim_first((CSIM_HEAD*)my.fronta);
    assert(zprava);
    csim_out((CSIM_LINK*)zprava);
    my.zpracovavanyPozadavek = zprava;
  }

  stats_zapocti(&my.periodaPruchoduPoz, 1, csim_time() - my.casPosledniAktivity); /* doba zpracovani pozadavku */

  my.casPosledniAktivity = csim_time();
  
  if (my.pouziteRozdeleni == EXP)
    csim_hold(random_negexp(my.mu)); /* naplanovani, simulace zpracovani pozadavku */
  else
    csim_hold(random_gauss_kladne(1.0 / my.mu, my.mu_odchylka)); /* naplanovani, simulace zpracovani pozadavku */
}
csim_end_process

/* vytvor kanal obsluhy s vazbou na danou frontu */
KANAL_OBSLUHY* kanal_obsluhy_vytvor(FRONTA* fronta, ROZDELENI pouziteRozdeleni, double mu, double mu_odchylka)
{
  KANAL_OBSLUHY* rtn = csim_new_process(KANAL_OBSLUHY, KANAL_OBSLUHY_PROG);
  assert(rtn);

  rtn->fronta = fronta;
  rtn->pouziteRozdeleni = pouziteRozdeleni;
  rtn->mu = mu;
  rtn->mu_odchylka = mu_odchylka;
  rtn->casPosledniAktivity = csim_time();
  rtn->zpracovavanyPozadavek = NULL;
  rtn->pokracovani.pointer = NULL;
  rtn->casAktivni = 0;

  stats_default(&rtn->stredniDobaZpracovani);
  stats_default(&rtn->periodaPruchoduPoz);
  stats_default(&rtn->dobaOdezvy);

  return rtn;
}

/* uvolni kanal obsluhy */
void kanal_obsluhy_uvolni(KANAL_OBSLUHY* kanal)
{
  csim_dispose_process((CSIM_PROCESS*)kanal);
}