/*
  Zprava putujici SHO - struktura a pridruzene funkce.
*/
#pragma once

#include "zaklad.h"

/*
  Definice zpravy putujici SHO.
*/
typedef struct 
{
  csim_d_link; /* macro - odvozeni od typu "link" */
  double casVytvoreni; /* cas vytvoreni zpravy */
  double casZarazeniDoFronty; /* cas posledniho zarazeni do fronty */
} ZPRAVA;

/* ------------------------------------------------------------------------ */

/* funkce pro vytvoreni zpravy */
ZPRAVA* zprava_vytvor();
/* funkce pro uvolneni zpravy */
int zprava_uvolni(ZPRAVA* zprava);