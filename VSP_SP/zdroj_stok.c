#include "zdroj_stok.h"
#include "zprava.h"

/* program zdroje pozadavku*/
csim_program(ZDROJ, ZDROJ_PROG)
for (;;) 
{
  /* generovani zpravy */
  ZPRAVA *zprava = zprava_vytvor();
  pokracuj(&my.pokracovani, zprava);

  csim_hold(my.pouziteRozdeleni == EXP ? random_negexp(my.lambda) : random_gauss_kladne(1.0 / my.lambda, my.lambda_odchylka)); /* naplanovani */
}
csim_end_process

/* ----------------------------------------------------------- */

/* vytvoreni zdroje */
ZDROJ* zdroj_vytvor(ROZDELENI pouziteRozdeleni, double lambda, double lambda_odchylka)
{
  ZDROJ* rtn = csim_new_process(ZDROJ, ZDROJ_PROG);
  assert(rtn);
  rtn->pouziteRozdeleni = pouziteRozdeleni;
  rtn->lambda = lambda;
  rtn->lambda_odchylka = lambda_odchylka;
  return rtn;
}

/* uvolneni zdroje */
void zdroj_uvolni(ZDROJ* zdroj)
{
  csim_dispose_process((CSIM_PROCESS*)zdroj);
}

/* vytvoreni stoku */
STOK* stok_vytvor()
{
  STOK* rtn = (STOK*)malloc(sizeof(STOK));
  assert(rtn);
  stats_default(&rtn->casPozadavek);
  rtn->casPosledniAktivity = csim_time();
  return rtn;
}

/* uvolneni stoku */
void stok_uvolni(STOK* stok)
{
  assert(stok);
  free(stok);
}

/* predani zpravy stoku */
int stok_pokracuj(STOK* stok, ZPRAVA* zprava)
{
  /* zapocteni statistik */
  stats_zapocti(&stok->casPozadavek, 1, csim_time() - zprava->casVytvoreni);

  double nyni = csim_time();
  stats_zapocti(&stok->periodaPruchoduPoz, 1, nyni - stok->casPosledniAktivity);
  stok->casPosledniAktivity = nyni;

  /* uvolneni zpravy */
  zprava_uvolni(zprava);
  return 0;
}