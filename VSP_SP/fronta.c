#include "fronta.h"

/* vytvor frontu */
FRONTA* fronta_vytvor()
{
  FRONTA* rtn = (FRONTA*)csim_new_head(FRONTA);
  assert(rtn);
  rtn->semaforFronta = csim_new_semaphore(0);
  rtn->casPosledniAktivity = csim_time();
  stats_default(&rtn->fronta_delka);
  return rtn;
}

/* vytvor frontu */
void fronta_uvolni(FRONTA* fronta)
{
  assert(fronta);
  csim_dispose_semaphore(fronta->semaforFronta);
  csim_dispose_head((CSIM_HEAD*)fronta);
}

/* predani pozadavku fronte */
void fronta_pokracuj(FRONTA* fronta, ZPRAVA* zprava)
{
  assert(fronta);
  assert(zprava);

  fronta_statistiky(fronta);
  csim_into((CSIM_LINK*)zprava, (CSIM_HEAD*)fronta); /* zarazeni zpravy do fronty */
  zprava->casZarazeniDoFronty = csim_time();

  csim_unlock_sem(fronta->semaforFronta); /* probuzeni kanalu obsluhy */
}

/* zapocteni statistik */
void fronta_statistiky(FRONTA* fronta)
{
  double nyni = csim_time();
  stats_zapocti(&fronta->fronta_delka, nyni - fronta->casPosledniAktivity, csim_cardinal((CSIM_HEAD*)fronta));
  fronta->casPosledniAktivity = nyni;
}