#include "krizovatka.h"

/* vytvor krizovatku */
KRIZOVATKA* krizovatka_vytvor()
{
  KRIZOVATKA* rtn = csim_new_head(KRIZOVATKA);
  assert(rtn);
  return rtn;
}

/* uvolni krizovatku */
void krizovatka_uvolni(KRIZOVATKA* krizovatka)
{
  assert(krizovatka);
  CSIM_LINK* moznost;
  while (NULL != (moznost = csim_first((CSIM_HEAD*)krizovatka)))
  {
    csim_dispose_link(moznost);
  }
  csim_dispose_head((CSIM_HEAD*)krizovatka);
}

/* pridani vystupu krizovatce
krizovatka - instance krizovatky
pokracovani - genericky pointer na navazujici strukturu
ppst - pravdepodobnost, ze se zprava vyda timto spojem
*/
void krizovatka_pridej_moznost(KRIZOVATKA* krizovatka, POKRACOVANI pokracovani, double ppst)
{
  assert(krizovatka);
  assert_pokracovani(pokracovani);
  assert(ppst >= 0 && ppst <= 1);

  KRIZOVATKA_MOZNOST* moznost;
  moznost = csim_new_link(KRIZOVATKA_MOZNOST);
  assert(moznost);
  moznost->pokracovani = pokracovani;
  moznost->ppst = ppst;

  csim_into((CSIM_LINK*)moznost, (CSIM_HEAD*)krizovatka);
}

/* predani zpravy krizovatce */
void krizovatka_pokracuj(KRIZOVATKA* pokr, ZPRAVA* zprava)
{
  KRIZOVATKA_MOZNOST* moznost = (KRIZOVATKA_MOZNOST*)csim_first((CSIM_HEAD*)pokr);
  assert(moznost); /* neni kam pokracovat! */

  double rnd = random_uniform(0, 1);
  double sum = 0;

  int found = 0;
  do
  {
    sum += moznost->ppst;
    if (sum >= rnd)
      found = 1;
    else if (moznost->suc)
      moznost = (KRIZOVATKA_MOZNOST*)moznost->suc;
    else
      found = 2;
  } while (!found);

  assert(found == 1);
  pokracuj(&moznost->pokracovani, zprava);
}