#include "zprava.h"

/* funkce pro vytvoreni zpravy */
ZPRAVA* zprava_vytvor()
{
  ZPRAVA* rtnZprava;
  rtnZprava = (ZPRAVA*)csim_new_link(ZPRAVA);
  assert(rtnZprava);
  rtnZprava->casVytvoreni = csim_time();
  rtnZprava->casZarazeniDoFronty = -1.0;
  return rtnZprava;
}

/* funkce pro uvolneni zpravy */
int zprava_uvolni(ZPRAVA* zprava)
{
  assert(zprava);
  csim_dispose_link((CSIM_LINK*)zprava);
  return 0;
}

