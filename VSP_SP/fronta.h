/* definice fronty - struktura, funkce*/
#pragma once

#include "zaklad.h"
#include "zprava.h"

typedef struct KANAL_OBSLUHY;

/* definice fronty */
typedef struct
{
  csim_d_head;
  CSIM_SEMAPHORE* semaforFronta; /* slouzi k blokovani kanalu obsluhy, je-li fronta prazdna */
  double casPosledniAktivity;  /* udava, kdy naposledy se s timto objektem neco delo */
  STATS fronta_delka; /* statistika delky fronty */
} FRONTA;

/* vytvor frontu */
FRONTA* fronta_vytvor();
/* vytvor frontu */
void fronta_uvolni(FRONTA* fronta);

/* zapocteni statistik */
void fronta_statistiky(FRONTA* fronta);
/* predani pozadavku fronte */
void fronta_pokracuj(FRONTA* fronta, ZPRAVA* zprava);