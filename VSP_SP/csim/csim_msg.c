/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_msg.c

                        C-Sim message passing system

*****************************************************************************/

#include "csim_msg.h"

static CSIM_HEAD *msg_queue = NULL;
static CSIM_HEAD *wp_queue = NULL;


CSIM_RESULT csim_init_msg()
{
  if (msg_queue != NULL && wp_queue != NULL)
    csim_clear_msg();
  if ((msg_queue = csim_new_head(CSIM_HEAD)) == NULL)
    return FAILURE;
  csim_set_name((CSIM_DYN_MEM *) msg_queue, "Message queue");
  if ((wp_queue = csim_new_head(CSIM_HEAD)) == NULL)
    return FAILURE;
  csim_set_name((CSIM_DYN_MEM *) wp_queue, "Message waiting process queue");
  return SUCCESS;
}


void csim_clear_msg()
{
  CSIM_LINK *p_link;

  while ((p_link = csim_first(msg_queue)) != NULL) {
    csim_dispose_link(p_link);
  }
  csim_dispose_head(msg_queue);
  msg_queue = NULL;

  while ((p_link = csim_first(wp_queue)) != NULL) {
    csim_dispose_link(p_link);
  }
  csim_dispose_head(wp_queue);
  wp_queue = NULL;
}


CSIM_MESSAGE *csim_init_message(CSIM_MESSAGE *msg)
{
  if (msg != NULL) {
    csim_init_link((CSIM_LINK *) msg);
    msg->msg_type = 0;
    msg->sender = CSIM_ANY_MSG_SENDER;
    msg->receiver = CSIM_ANY_MSG_RECEIVER;
  }
  return msg;
}


void csim_send_msg(CSIM_MESSAGE *msg,
                   CSIM_ULONG msg_type,
                   CSIM_PROCESS *receiver)
{
  CSIM_WAITING_PROCESS *p_wp;
  CSIM_BOOLEAN in_wp_queue = FALSE;

  msg->msg_type = msg_type;
  msg->sender   = csim_current();
  msg->receiver = receiver;

  if ((p_wp = (CSIM_WAITING_PROCESS *) csim_first(wp_queue)) != NULL) {
    do {
      if (csim_test_msg(msg, p_wp->msg_type, p_wp->sender, 
        p_wp->receiver) == TRUE) {
        *(p_wp->p_msg) = msg;
        csim_activate_at(p_wp->receiver, csim_time());
        csim_out((CSIM_LINK *) p_wp);
        csim_dispose_link((CSIM_LINK *) p_wp);
        p_wp = NULL;
        in_wp_queue = TRUE;
      }
      else
        p_wp = (CSIM_WAITING_PROCESS *) p_wp->suc;
    } while ((in_wp_queue == FALSE)
      && (p_wp != (CSIM_WAITING_PROCESS *) csim_first(wp_queue)));
  }
  if (in_wp_queue == FALSE)
    csim_into((CSIM_LINK *) msg, msg_queue);
}


CSIM_BOOLEAN csim_test_msg(CSIM_MESSAGE *msg,
                           CSIM_ULONG msg_type,
                           CSIM_PROCESS *sender,
                           CSIM_PROCESS *receiver)
{
  if ((msg->msg_type & msg_type)
    && ((msg->receiver == CSIM_ANY_MSG_RECEIVER) || (msg->receiver == receiver))
    && ((sender == CSIM_ANY_MSG_SENDER) || (msg->sender == sender)))
    return TRUE;
  else
    return FALSE;
}


CSIM_HEAD *csim_msg_queue_point()
{
  return msg_queue;
}


CSIM_HEAD *csim_wp_queue_point()
{
  return wp_queue;
}

