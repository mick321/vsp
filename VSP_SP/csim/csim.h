/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                03.04.2003

                             C-Sim version 5.1

                               file csim.h

                        header file of C-Sim library
                (all exported functions and global variables)

*****************************************************************************/

#ifndef CSIM_H
#define CSIM_H 1

#ifndef KR_PATH
#define KR_PATH "kr_jmp/csim_kr.h"
#endif

#include <stdlib.h>
#include "csim_dt.h"

#include KR_PATH
#include "csim_tm.h"


/*****************************************************************************
                                  used macros
*****************************************************************************/

/*
 *  Use following macros like this:
 *
 *  typedef struct my_link {
 *    csim_d_link;
 *      ...
 *    user attributes;
 *      ...
 *  } MY_LINK;
 */

/*
 * Class for block of dynamic memory
 * name       - used by the debug function - user can use this to identify
 *              object
 * type       - object type
 *              P ... process
 *              L ... csim_link
 *              H ... csim_head
 *              M ... dynamic memory block
 * check      - pointer used for checks
 * p_head_mem - pointer to head of list of dynamically created objects
 * p_suc_mem  - pointer to successor in the list
 * p_pred_mem - pointer to predecessor in the list
 * destructor - pointer to object's destructor
 * view       - function for viewing object's data
 */

/*
 * Macro for derivation from the CSIM_DYN_MEM type
 * class of dynamically generated objects which are
 * inserted into one list
 */

#define csim_d_dyn_mem               \
  char                 *name;        \
  char                 type;         \
  char                 *check;       \
  struct csim_dyn_mem  *p_head_mem;  \
  struct csim_dyn_mem  *p_suc_mem;   \
  struct csim_dyn_mem  *p_pred_mem;  \
  CSIM_DESTRUCTOR      destructor;   \
  CSIM_VIEW            view

/*
 * Class for objects insertable into list
 * suc        - successor in the list
 * pred       - predecessor in the list
 * head       - head of the list
 * enter_time - time of the first insertion to the queue
 */

/*
 * Macro for derivation from the CSIM_LINK type objects insertable
 * into the CSIM_HEAD list
 */
#define csim_d_link                  \
  csim_d_dyn_mem;                    \
  struct csim_link     *suc;         \
  struct csim_link     *pred;        \
  struct csim_head     *head;        \
  CSIM_TIME            enter_time

/*
 * Class for list head objects, also insertable into list
 * p_first     - pointer to the first item in the list
 * p_last      - pointer to the last item in the list
 * lq          - actual list length
 * arrival_cnt - number of items inserted into the list during the time
 *               statistics was turned on
 * last_time   - time of the last change of list length
 * sum_tw      - sum of the item's waiting times
 * sum_lw      - sum of the actual list lengths weight by the time their last
 * sum_time    - total time the statistics was on (used to count Lw)
 * statistics  - enables/disables the statistics
 *             - if the global variable statistics is TRUE, the statistics
 *               are evaluated always
 *             - if the global variable statistics is FALSE, the statistics
 *               are only evaluated when this attribute is TRUE
 */

/*
 * Macro for derivation from the CSIM_HEAD type the head of the list
 */
#define csim_d_head                   \
  csim_d_link;                        \
  CSIM_LINK       *p_first;           \
  CSIM_LINK       *p_last;            \
  CSIM_UWORD      lq;                 \
  CSIM_ULONG      arrival_cnt;        \
  CSIM_TIME       last_time;          \
  CSIM_TIME       sum_tw;             \
  CSIM_TIME       sum_lw;             \
  CSIM_TIME       sum_time;           \
  CSIM_BOOLEAN    statistics

/*
 * Class for process objects
 * program          - pointer to program of the process. It is run after the
 *                    process was started (e.g. is in state NEW_...)
 * suc_in_sqs       - pointer to the next process in scheduler
 * pred_in_sqs      - pointer to previous process in scheduler
 * state            - state of the process
 * evtime           - time of the process's activation. It determines position
 *                    of the process in scheduler
 * last_time        - the time when the process was last inserted into
 *                    scheduler
 * non_passive_time - sum of time intervals when the process was scheduled
 * passive_time     - sum of time intervals when the process was passive
 * statistics       - enables/disables the statistics
 *                  - if the global variable statistics is TRUE, the statistics
 *                    are evaluated always
 *                  - if the global variable statistics is FALSE, the statistics
 *                    are only evaluated when this attribute is TRUE
 */

/*
 * Macro for derivation from the CSIM_PROCESS type object of one process
 */
#define csim_d_process                    \
  csim_d_link;                            \
  csim_process_kr;                        \
  CSIM_PROC_PROGRAM    program;           \
  struct csim_process  *suc_in_sqs;       \
  struct csim_process  *pred_in_sqs;      \
  CSIM_PROC_STATE      state;             \
  CSIM_TIME            evtime;            \
  CSIM_TIME            last_time;         \
  CSIM_TIME            non_passive_time;  \
  CSIM_TIME            passive_time;      \
  CSIM_BOOLEAN         statistics

/*
 * The macros with the same function as above, but with better readability
 *
 * Use like this:
 * csim_derived_link(my_link)
 *     ...
 *   user attributes;
 *     ...
 * csim_end_derived(MY_LINK);
 */


/*
 * !!!!!!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!!!!!
 *
 *  The parameter TYPE_NAME (macros derived_xyz(TYPE_NAME)
 *  and csim_end_derived(TYPE_NAME)) are the names of one and
 *  the same structure. Please use
 *
 *  lower case (like type_name) when using macro derived_xyz
 *  it is used by the structure for selfreferencing
 *  upper case (like TYPE_NAME) when using macro csim_end_derived
 *  it is used as structure's name
 *
 *  Example:                          Expands to:
 *  csim_derived_link(my_link)        typedef struct my_link {
 *      ...                             csim_d_link;
 *    my_attributes;                    my_attributes;
 *    struct my_link *p_my_link;        struct my_link *p_my_link;
 *      ...                               ...
 *  csim_end_derived(MY_LINK)         } MY_LINK;
 */

#define csim_derived_dyn_mem(TYPE_NAME) typedef struct TYPE_NAME {            \
                                     csim_d_dyn_mem;

#define csim_derived_link(TYPE_NAME) typedef struct TYPE_NAME {               \
                                  csim_d_link;

#define csim_derived_head(TYPE_NAME) typedef struct TYPE_NAME {               \
                                  csim_d_head;

#define csim_derived_process(TYPE_NAME) typedef struct TYPE_NAME {            \
                                     csim_d_process;

#define csim_end_derived(TYPE_NAME) } TYPE_NAME


/*
 * Macros for dynamic generation of new objects
 *
 * TYPE_NAME - name of the type - CSIM_LINK, MY_LINK, CSIM_HEAD, MY_HEAD ...
 * PROC_PROG - pointer to program of the process
 *
 * Usage:
 * csim_derived_process(my_process)
 *            ...
 * csim_end_derived(MY_PROCESS);
 *
 * void prog_of_process(void) {
 *            ...
 * }
 *
 * void main(void) {
 *   MY_PROCESS *p_proc;
 *
 *   p_proc = new_process(MY_PROCESS, prog_of_process);
 *            ...
 * }
 */

/* Generates new object derived from CSIM_DYN_MEM */
#define csim_new_dyn_mem(TYPE_NAME)                                           \
  (TYPE_NAME *) csim_init_dyn_mem((CSIM_DYN_MEM *) malloc(sizeof(TYPE_NAME)))

/* Generates new object derived from CSIM_LINK */
#define csim_new_link(TYPE_NAME)                                              \
  (TYPE_NAME *) csim_init_link((CSIM_LINK *) malloc(sizeof(TYPE_NAME)))

/* Generates new object derived from CSIM_HEAD */
#define csim_new_head(TYPE_NAME)                                              \
  (TYPE_NAME *) csim_init_head((CSIM_HEAD *) malloc(sizeof(TYPE_NAME)))

/* Generates new object derived from CSIM_PROCESS */
#define csim_new_process(TYPE_NAME, PROC_PROG)                                \
  (TYPE_NAME *) csim_init_process(                                            \
    (CSIM_PROCESS *) malloc(sizeof(TYPE_NAME)), (PROC_PROG))

/*
 * Macros that initialize the attributes of CSIM_DYN_MEM type
 *
 * CSIM_DYN_MEM *P_DYN_MEM    - pointer to object
 * char *NAME                 - object's name (identification string)
 * CSIM_DESTRUCTOR DESTRUCTOR - pointer to object's destructor
 * CSIM_VIEW VIEW             - pointer to function used to view the object
 *                              (is of type CSIM_VIEW)
 */

#define csim_set_destructor(P_DYN_MEM, DESTRUCTOR)                            \
  (P_DYN_MEM)->destructor = (DESTRUCTOR)

#define csim_set_view(P_DYN_MEM, VIEW) (P_DYN_MEM)->view = (VIEW)

/* Macro used to access object's attributes */
#define my (*p_my)

/* Macro used to generate error code */
#define CSIM_ERR_CODE(func, err) (CSIM_UWORD) ((err << 8) | func)

/*
 * Next four macros are working with active process and are changing it's state:
 *   - csim_hold(del_t)   : changes the state of process to CSIM_PLANNED
 *                          and schedules it to current time plus del_t
 *   - csim_passivate()   : changes the process to state PASSIVE
 *   - csim_wait(p_head)  : changes the process to state PASSIVE and inserts it
 *                          into list p_head
 *   - csim_end_program   : ends program code
 */

#define csim_hold(del_t)                                                      \
do {                                                                          \
  CSIM_PROCESS *_p_process;                                                   \
  CSIM_TIME _csim_dt;                                                         \
  _p_process = csim_current();                                                \
  _csim_dt = (del_t);                                                         \
                                                                              \
  csim_sqs_out(_p_process);                                                   \
  if (csim_time_cmp(_csim_dt, csim_zero_time()) >= 0)                         \
    _p_process->evtime = csim_time_add(_p_process->evtime, _csim_dt);         \
  else                                                                        \
    csim_exception(CSIM_E_HOLD_DEL_T, _p_process, NULL);                      \
  csim_rank(_p_process);                                                      \
  _p_process->state = CSIM_PLANNED;                                           \
                                                                              \
  csim_compute_deactivate_ps(_p_process);                                     \
  _p_process->last_time = csim_time();                                        \
                                                                              \
  csim_switch_processes(_p_process, csim_sqs_point());                        \
  csim_restore_process;                                                       \
} while (0)

#define csim_passivate()                                                      \
do {                                                                          \
  CSIM_PROCESS *_p_process;                                                   \
  _p_process = csim_current();                                                \
                                                                              \
  csim_sqs_out(_p_process);                                                   \
  _p_process->state = CSIM_PASSIVE;                                           \
                                                                              \
  csim_compute_deactivate_ps(_p_process);                                     \
  _p_process->last_time = csim_time();                                        \
                                                                              \
  csim_switch_processes(_p_process, csim_sqs_point());                        \
  csim_restore_process;                                                       \
} while (0)

#define csim_wait(p_head)                                                     \
do {                                                                          \
  CSIM_PROCESS *_p_process;                                                   \
  CSIM_HEAD *_csim_p_head;                                                    \
  _p_process = csim_current();                                                \
  _csim_p_head = (p_head);                                                    \
                                                                              \
  if (csim_check_head(_csim_p_head) == TRUE) {                                \
    csim_into(((CSIM_LINK *) _p_process), p_head);                            \
                                                                              \
    csim_sqs_out(_p_process);                                                 \
    _p_process->state = CSIM_PASSIVE;                                         \
                                                                              \
    csim_compute_deactivate_ps(_p_process);                                   \
    _p_process->last_time = csim_time();                                      \
  }                                                                           \
  else {                                                                      \
    csim_exception(CSIM_ERR_CODE(CSIM_E_WAIT_HEAD,                            \
     csim_head_state(_csim_p_head)), _p_process, _csim_p_head);               \
  }                                                                           \
                                                                              \
  csim_switch_processes(_p_process, csim_sqs_point());                        \
  csim_restore_process;                                                       \
} while (0)

#define csim_end_program                                                      \
{                                                                             \
  CSIM_PROCESS *_p_process;                                                   \
  _p_process = csim_current();                                                \
                                                                              \
  csim_sqs_out(_p_process);                                                   \
  _p_process->state = CSIM_TERMINATED;                                        \
                                                                              \
  csim_compute_deactivate_ps(_p_process);                                     \
  _p_process->last_time = csim_time();                                        \
                                                                              \
  csim_return_to_step();                                                      \
}                                                                             \
}

/* Macros-brackets creating a special kind of functions */
#define csim_program(TYPE_NAME, PROG_NAME)                                    \
void *PROG_NAME(void *p_void)                                                 \
{                                                                             \
  TYPE_NAME *p_my = (TYPE_NAME *) p_void;

#define csim_end_process                                                      \
{                                                                             \
  CSIM_PROCESS *_p_process;                                                   \
  _p_process = csim_current();                                                \
                                                                              \
  csim_sqs_out(_p_process);                                                   \
  _p_process->state = CSIM_ZOMBIE;                                            \
                                                                              \
  csim_return_to_step();                                                      \
}                                                                             \
}

#define csim_begin_destructor(TYPE_NAME, DESTR_NAME)                          \
void DESTR_NAME(void *p_void)                                                 \
{                                                                             \
  TYPE_NAME *p_my = (TYPE_NAME *) p_void;

#define csim_end_destructor                                                   \
}

#define csim_begin_view(TYPE_NAME, VIEW_NAME)                                 \
void VIEW_NAME(void *p_void)                                                  \
{                                                                             \
  TYPE_NAME *p_my = (TYPE_NAME *) p_void;

#define csim_end_view                                                         \
}


/*****************************************************************************
                          classes of basic objects
*****************************************************************************/

/*
 * enumeration type - the states of process
 * CSIM_NEW_PASSIVE - new passive process
 *                    needed to recognize if we will run program
 *                    of process from the beginning or from
 *                    the reactivation point
 * CSIM_NEW_PLANNED - new scheduled process
 *                    needed to recognize if we will run program
 *                    of process from the beginning or from
 *                    the reactivation point
 * CSIM_PLANNED     - scheduled process run from
 *                    the reactivation point
 * CSIM_ACTIVE      - currently active process
 * CSIM_PASSIVE     - process is waiting to be scheduled
 * CSIM_TERMINATED  - program of process has ended
 * CSIM_ZOMBIE      - program of process has ended and process is disposed
 *                    after return to step
 */

typedef enum {
  CSIM_NEW_PASSIVE, CSIM_NEW_PLANNED, CSIM_PLANNED, CSIM_ACTIVE,
  CSIM_PASSIVE, CSIM_TERMINATED, CSIM_ZOMBIE
} CSIM_PROC_STATE;

/* enumeration type for states of CSIM_DYN_MEM type - used for internal checks
 * CSIM_D_IS_RANK    - object is probably inserted into the list of dynamic
 *                     blocs. We can only be sure that p_suc_mem, p_pred_mem
 *                     and p_head_mem are not NULL.
 * CSIM_D_NOT_RANK   - object is not inserted into the list of dynamic blocs
 * CSIM_D_DEFECTIVE  - object is not in any of the previous states
 * CSIM_D_ILLEGAL    - pointer to object does not correspond to control pointer
 *                     char *check set by object's generation
 * CSIM_D_IS_NULL    - pointer to checked object is NULL
 */

typedef enum {
  CSIM_D_DEFECTIVE, CSIM_D_ILLEGAL, CSIM_D_IS_NULL,
  CSIM_D_IS_RANK, CSIM_D_NOT_RANK
} CSIM_DYN_MEM_CHECK;

/* enumeration type for states of CSIM_LINK object - used for internal checks
 * CSIM_L_IS_RANK    - object is probably inserted into the list of dynamic
 *                     blocs. We can only be sure that suc, pred and head
 *                     are not NULL.
 * CSIM_L_NOT_RANK   - object is not inserted into any list
 * CSIM_L_DEFECTIVE  - object in not in some of previous states
 * CSIM_L_ILLEGAL    - pointer to object does not correspond to control pointer
 *                     char *check set by object's generation
 * CSIM_L_IS_NULL    - pointer to checked object is NULL
 */

typedef enum {
  CSIM_L_DEFECTIVE, CSIM_L_ILLEGAL, CSIM_L_IS_NULL,
  CSIM_L_IS_RANK, CSIM_L_NOT_RANK
} CSIM_LINK_CHECK;

/*
 * enumeration type for states of CSIM_HEAD object - used for internal checks
 * CSIM_H_EMPTY             - the list is empty - p_first and p_last are NULL
 * CSIM_H_NOT_EMPTY         - the list is probably not empty. We can only be sure
 *                     that p_first and p_last are not NULL.
 * CSIM_H_DEFECTIVE       - object is not in any of the previous states
 * CSIM_H_ILLEGAL - pointer to object does not correspond to control pointer
 *                     char *check set by object's generation
 * CSIM_H_IS_NULL         - pointer to checked object is NULL
 */

typedef enum {
  CSIM_H_DEFECTIVE, CSIM_H_ILLEGAL, CSIM_H_IS_NULL,
  CSIM_H_EMPTY, CSIM_H_NOT_EMPTY
} CSIM_HEAD_CHECK;

/*
 * enumeration type for states of CSIM_LINK object - used for internal checks
 * CSIM_P_IS_RANK    - process is in the scheduler. Sure is only that
 *                     suc_in_sqs and pred_in_sqs are not NULL
 * CSIM_P_NOT_RANK   - object is not in the scheduler - suc_in_sqs and
 *                     pred_in_sqs are NULL
 * CSIM_P_DEFECTIVE  - object is not in any of the previous states
 * CSIM_P_ILLEGAL    - pointer to object does not correspond to control pointer
 *                     char *check set by object's generation
 * CSIM_P_IS_NULL    - pointer to checked object is NULL
 */

typedef enum {
  CSIM_P_DEFECTIVE, CSIM_P_ILLEGAL, CSIM_P_IS_NULL, CSIM_P_IS_TERMINATED,
  CSIM_P_IS_RANK, CSIM_P_NOT_RANK
} CSIM_PROC_CHECK;


typedef void *((*CSIM_PROC_PROGRAM) (void *p_void)); /* program of process    */
typedef void ((*CSIM_DESTRUCTOR) (void *p_void));    /* destructor            */
typedef void ((*CSIM_VIEW) (void *p_void));          /* function used to view */

/*
 * Types of basic objects
 */

typedef struct csim_dyn_mem {
  csim_d_dyn_mem;
} CSIM_DYN_MEM;

typedef struct csim_link {
  csim_d_link;
} CSIM_LINK;

typedef struct csim_head {
  csim_d_head;
} CSIM_HEAD;

typedef struct csim_process {
  csim_d_process;
} CSIM_PROCESS;

/*
 * Enumeration type for error code generation
 */

typedef enum {
  CSIM_E_SET_NAME_DYN_MEM = 1,
  CSIM_E_FIRST_HEAD,       /* csim_first()                  */
  CSIM_E_LAST_HEAD,        /* csim_last()                   */
  CSIM_E_EMPTY_HEAD,       /* csim_empty()                  */
  CSIM_E_CARD_HEAD,        /* csim_cardinal()               */
  CSIM_E_CLEAR_HEAD,       /* csim_clear()                  */
  CSIM_E_OUT_LINK,         /* csim_out()                    */
  CSIM_E_INTO_LINK,        /* csim_into()                   */
  CSIM_E_INTO_HEAD,
  CSIM_E_FOLL_WHAT,        /* csim_follow()                 */
  CSIM_E_FOLL_WHERE,
  CSIM_E_PREC_WHAT,        /* csim_precede()                */
  CSIM_E_PREC_WHERE,
  CSIM_E_WAIT_HEAD,        /* csim_wait()                   */
  CSIM_E_DISP_DYN_MEM,     /* csim_dispose_dyn_mem()        */
  CSIM_E_DISP_LINK,        /* csim_dispose_link()           */
  CSIM_E_DISP_HEAD,        /* csim_dispose_head()           */
  CSIM_E_DISP_PROC,        /* csim_dispose_process()        */
  CSIM_E_STATE_PROC,       /* csim_state()                  */
  CSIM_E_IDLE_PROC,        /* csim_idle()                   */
  CSIM_E_EVTIME_PROC,      /* csim_evtime()                 */
  CSIM_E_CANCEL_PROC,      /* csim_cancel()                 */
  CSIM_E_ACT_AT_PROC,      /* csim_activate_at()            */
  CSIM_E_ACT_DEL_PROC,     /* csim_activate_delay()         */
  CSIM_E_REACT_AT_PROC,    /* csim_reactivate_at()          */
  CSIM_E_REACT_DEL_PROC,   /* csim_reactivate_delay()       */
  CSIM_E_HSTAT_ON_HEAD,    /* csim_h_stat_on()              */
  CSIM_E_HSTAT_OFF_HEAD,   /* csim_h_stat_off()             */
  CSIM_E_HSTAT_INIT_HEAD,  /* csim_init_h_stat()            */
  CSIM_E_HSTAT_ST_HEAD,    /* csim_h_stat_status()          */
  CSIM_E_HSTAT_HEAD,       /* csim_h_stat()                 */
  CSIM_E_PSTAT_ON_PROC,    /* csim_p_stat_on()              */
  CSIM_E_PSTAT_OFF_PROC,   /* csim_h_stat_off()             */
  CSIM_E_PSTAT_INIT_PROC,  /* csim_init_h_stat()            */
  CSIM_E_PSTAT_ST_PROC,    /* csim_h_stat_status()          */
  CSIM_E_PSTAT_PROC,       /* csim_h_stat()                 */

  CSIM_E_ERROR_CODE_SEPARATOR,

  CSIM_E_INTO_LINK_IS_IN,
  CSIM_E_FOLL_WHAT_IS_IN, CSIM_E_FOLL_WHERE_NOT_IN,
  CSIM_E_PREC_WHAT_IS_IN, CSIM_E_PREC_WHERE_NOT_IN,
  CSIM_E_HOLD_DEL_T,
  CSIM_E_INIT_PROC_PROG_NULL,
  CSIM_E_DISP_PROC_ACTIVE,
  CSIM_E_STEP_EMPTY,
  CSIM_E_CANCEL_NOT_RANK, CSIM_E_CANCEL_IS_ACTIVE,
  CSIM_E_ACT_AT_T, CSIM_E_ACT_AT_IS_RANK, CSIM_E_ACT_AT_IS_ACTIVE,
  CSIM_E_ACT_DEL_T, CSIM_E_ACT_DEL_IS_RANK, CSIM_E_ACT_DEL_IS_ACTIVE,
  CSIM_E_REACT_AT_T, CSIM_E_REACT_AT_IS_ACTIVE,
  CSIM_E_REACT_DEL_T, CSIM_E_REACT_DEL_IS_ACTIVE,
  CSIM_E_CREATE_PROCESS,

  CSIM_USER_ERROR_CODE
} CSIM_ERROR_CODE;

/*
 * Structure describing the error - filled by function error(...)
 *
 * error_code   - code of the error
 * p_proc_error - pointer to process that was active when
 *                the error occured
 * p_void       - pointer to object that caused the error
 *                (the debug function can be used to view the object)
 *                user can define his own errors - in that case there
 *                can be pointer to user-defined structure
 */

typedef struct {
  CSIM_UWORD      error_code;
  CSIM_PROCESS    *p_proc_error;
  void            *p_void;
} CSIM_ERROR;


/*
 * Initialization functions and functions for dynamic allocation
 */

/* Memory initialization */
extern CSIM_RESULT csim_init_mem(void);

/* Clears the list of dynamic blocks */
extern void csim_clear_mem(void);

/* Initializes the CSIM_DYN_MEM object */
extern CSIM_DYN_MEM *csim_init_dyn_mem(CSIM_DYN_MEM *p_dyn_mem);

/* Initializes the CSIM_LINK object */
extern CSIM_LINK *csim_init_link(CSIM_LINK *p_link);

/* Initializes the CSIM_HEAD object */
extern CSIM_HEAD *csim_init_head(CSIM_HEAD *p_head);

/* Initializes the CSIM_PROCESS object */
extern CSIM_PROCESS *csim_init_process(CSIM_PROCESS *p_process,
                                       CSIM_PROC_PROGRAM proc_prog);

/* Releases the CSIM_DYN_MEM object's memory */
extern CSIM_RESULT csim_dispose_dyn_mem(CSIM_DYN_MEM *p_dyn_mem);

/* Releases the CSIM_LINK object's memory */
extern CSIM_RESULT csim_dispose_link(CSIM_LINK *p_link);

/* Releases the CSIM_HEAD object's memory */
extern CSIM_RESULT csim_dispose_head(CSIM_HEAD *p_head);

/* Releases the CSIM_PROCESS object's memory */
extern CSIM_RESULT csim_dispose_process(CSIM_PROCESS *p_process);

extern CSIM_RESULT csim_set_name(CSIM_DYN_MEM *p_dyn_mem, char *name);

/*
 * Functions that support the CSIM_LINK object
 */

/* Removes the item prom the list */
extern CSIM_RESULT csim_out(CSIM_LINK *p_link);

/* Inserts the item into the list on the last place */
extern CSIM_RESULT csim_into(CSIM_LINK *p_link,
                      CSIM_HEAD *p_head);

/* Inserts the item into the list as successor of another item */
extern CSIM_RESULT csim_follow(CSIM_LINK *p_link_what,
                               CSIM_LINK *p_link_where);

/* Inserts the item into the list as predecessor of another item */
extern CSIM_RESULT csim_precede(CSIM_LINK *p_link_what,
                                CSIM_LINK *p_link_where);

/*
 * Functions that support the CSIM_HEAD object
 */

/* Returns the first item in the list */
extern CSIM_LINK *csim_first(CSIM_HEAD *p_head);

/* Returns the last item in the list */
extern CSIM_LINK *csim_last(CSIM_HEAD *p_head);

/* Returns TRUE if the list is empty */
extern CSIM_BOOLEAN csim_empty(CSIM_HEAD *p_head);

/* Returns actual list length */
extern CSIM_WORD csim_cardinal(CSIM_HEAD *p_head);

/* Empties the list */
extern CSIM_RESULT csim_clear(CSIM_HEAD *p_head);

/*
 * Functions that support process and simulation control
 */

/* Schedules process on time t */
extern CSIM_RESULT csim_activate_at(CSIM_PROCESS *p_process,
                                    CSIM_TIME t);

/* Scheduler process on current time plus del_t */
extern CSIM_RESULT csim_activate_delay(CSIM_PROCESS *p_process,
                                       CSIM_TIME del_t);

/* Rechedules process on time t */
extern CSIM_RESULT csim_reactivate_at(CSIM_PROCESS *p_process,
                                      CSIM_TIME t);

/* Reschedules process on current time plus del_t */
extern CSIM_RESULT csim_reactivate_delay(CSIM_PROCESS *p_process,
                                         CSIM_TIME del_t);

/* Removes process from scheduler */
extern CSIM_RESULT csim_cancel(CSIM_PROCESS *p_process);

/* Returns the state of process */
extern CSIM_PROC_STATE csim_state(CSIM_PROCESS *p_process);

/* Tests the process state - returns TRUE if the process is passive */
extern CSIM_BOOLEAN csim_idle(CSIM_PROCESS *p_process);

/* Returns the process scheduled time */
extern CSIM_TIME csim_evtime(CSIM_PROCESS *p_process);

/* Returns pointer to the second process in scheduler */
extern CSIM_PROCESS *csim_next_proc(void);

/* Realizes one step of simulation alghoritm */
extern CSIM_RESULT csim_step(void);

/* Returns actual model time */
extern CSIM_TIME csim_time(void);

/* Returns pointer to currently active */
extern CSIM_PROCESS *csim_current(void);

/* Returns pointer to the head of the scheduler's list */
extern CSIM_PROCESS *csim_sqs_point(void);

/* Returns pointer to the head of the heap */
extern CSIM_DYN_MEM *csim_mem_point(void);

/*
 * Check functions
 */

/* Returns control state of the CSIM_DYN_MEM object */
extern CSIM_DYN_MEM_CHECK csim_dyn_mem_state(CSIM_DYN_MEM *p_dyn_mem);

/* Returns control state of the CSIM_LINK object */
extern CSIM_LINK_CHECK csim_link_state(CSIM_LINK *p_link);

/* Returns control state of the CSIM_HEAD object */
extern CSIM_HEAD_CHECK csim_head_state(CSIM_HEAD *p_head);

/* Returns control state of the CSIM_PROCESS object */
extern CSIM_PROC_CHECK csim_process_state(CSIM_PROCESS *p_process);

/* Checks the CSIM_DYN_MEM object */
extern CSIM_BOOLEAN csim_check_dyn_mem(CSIM_DYN_MEM *p_dyn_mem);

/* Checks the CSIM_PROCESS object */
extern CSIM_BOOLEAN csim_check_process(CSIM_PROCESS *p_process);

/* Checks the CSIM_HEAD object */
extern CSIM_BOOLEAN csim_check_head(CSIM_HEAD *p_head);

/* Checks the CSIM_LINK object */
extern CSIM_BOOLEAN csim_check_link(CSIM_LINK *p_link);

/*
 * Statistics functions
 */

/* Turns the global statistics on */
extern void csim_stat_on(void);

/* Turns the global statistics off */
extern void csim_stat_off(void);

/* Returns the state of global statistics - TRUE if on */
extern CSIM_BOOLEAN csim_stat_status(void);

/* Turns the local statistics of the object CSIM_HEAD on */
extern CSIM_RESULT csim_h_stat_on(CSIM_HEAD *p_head);

/* Turns the local statistics of the object CSIM_HEAD off */
extern CSIM_RESULT csim_h_stat_off(CSIM_HEAD *p_head);

/* Initiates the statistics variables */
extern CSIM_RESULT csim_init_h_stat(CSIM_HEAD *p_head);

/* Returns state of the local statistics - TRUE if on */
extern CSIM_BOOLEAN csim_h_stat_status(CSIM_HEAD *p_head);

/* Returns statistics results */
extern CSIM_RESULT csim_h_stat(CSIM_HEAD *p_head,
                               double *p_Lw,
                               double *p_Tw);

/* Turns the local statistics of the CSIM_PROCESS object on */
extern CSIM_RESULT csim_p_stat_on(CSIM_PROCESS *p_process);

/* Turns the local statistics of the CSIM_PROCESS object off */
extern CSIM_RESULT csim_p_stat_off(CSIM_PROCESS *p_process);

/* Initiates the statistics variables */
extern CSIM_RESULT csim_init_p_stat(CSIM_PROCESS *p_process);

/* Returns state of the local statistics - TRUE if on */
extern CSIM_BOOLEAN csim_p_stat_status(CSIM_PROCESS *p_process);

/* Returns statistics results */
extern CSIM_RESULT csim_p_stat(CSIM_PROCESS *p_process,
                               double *p_Ts,
                               double *p_Tr);

/*
 * Error functions
 */

/* error indication */
extern CSIM_BOOLEAN csim_error_status(void);

/* sets the error structure with actual error and returns it */
extern void csim_error(CSIM_ERROR *p_error);

/*
 * Functions used in macros, DO NOT USE DIRECTLY!
 */

/* Inserts process into scheduler on place determined by the value of evtime */
extern void csim_rank(CSIM_PROCESS *p_process);

/* Removes process from calendar */
extern void csim_sqs_out(CSIM_PROCESS *p_process);

/* compute process statistics */
extern void csim_compute_deactivate_ps(CSIM_PROCESS *p_process);

/* sets the error and ends the simulation step */
extern void csim_exception(CSIM_UWORD error_code,
                           CSIM_PROCESS *p_proc,
                           void *p_void);

/*
 * Global variables
 */

/* counter of simulation steps */
extern unsigned long csim_step_number;

#endif
