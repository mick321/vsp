/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_msg.h

                header file of C-Sim message passing system

*****************************************************************************/

#ifndef CSIM_MSG_H
#define CSIM_MSG_H 1

#include "csim.h"

/* Constants used in message transmissison */
#define CSIM_ANY_MSG_RECEIVER  NULL
  /* The message can be received by any csim_process                    */
#define CSIM_ANY_MSG_SENDER    NULL
  /* The receiver accepts messages from any server                      */
#define CSIM_ANY_MSG_TYPE      0xFFFFFFFFL
  /* This mask ensures the reception of any message type and vice versa */

/* Macro for derivation of user defined message type */
#define csim_d_message         \
  csim_d_link;                 \
  CSIM_ULONG       msg_type;   \
  CSIM_PROCESS     *sender;    \
  CSIM_PROCESS     *receiver

#define csim_derived_message(TYPE_NAME) typedef struct TYPE_NAME {            \
                                          csim_d_message;

/*
 * Standard message type
 * Use like this:
 *
 * csim_program(CSIM_PROCESS, sender_program)
 * {
 *   CSIM_MESSAGE *msg;
 *
 *   msg = csim_new_message(CSIM_MESSAGE);
 *   csim_send_msg(msg, 1, CSIM_ANY_MSG_RECEIVER);
 * }
 *
 * csim_program(CSIM_PROCESS, receiver_program)
 * {
 *
 *   csim_receive_msg(my.msg, 1, CSIM_ANY_MSG_SENDER);
 *   csim_dispose_message(msg);
 * }
 */
typedef struct csim_message {
  csim_d_message;
} CSIM_MESSAGE;

/* For internal use only - DO NOT USE */
typedef struct csim_waiting_process {
  csim_d_link;
  CSIM_ULONG    msg_type;
  CSIM_PROCESS  *sender;
  CSIM_PROCESS  *receiver;
  CSIM_MESSAGE  **p_msg;
} CSIM_WAITING_PROCESS;

/*
 * Initialization of the Message-passing module. This function should be
 * called exactly once - before other function.
 * Before exiting the function "csim_clear_msg()" should be called.
 */
extern CSIM_RESULT csim_init_msg();

/* Frees all alocated resources by this module. */
extern void csim_clear_msg();

/*
 * Dynamicaly creates (allocates memory) and initializes a new message.
 * The TYPE_NAME argument can be instance of any structure derived using
 * the "csim_d_message" macro.
 */
#define csim_new_message(TYPE_NAME)                                           \
  (TYPE_NAME *) csim_init_message((CSIM_MESSAGE *) malloc(sizeof(TYPE_NAME)));

/*
 * Frees the resources allocated to the specified message. The message must
 * have been allocated dynamicaly using the csim_new_message macro.
 */
#define csim_dispose_message(msg)                                             \
  csim_dispose_link((CSIM_LINK *) msg);

/*
 * Initializes any user-derived message with the default starting values.
 * Must be called before any use of the message.
 */
extern CSIM_MESSAGE *csim_init_message(CSIM_MESSAGE *msg);

/*
 * Sends the specified message "msg" to the "receiver" process. The message
 * will be received only if the bitwise AND of sender and receiver "msg_type"
 * gives a nonzero result. If the "receiver" argument is CSIM_ANY_MSG_RECEIVER
 * the message is received by the first available process
 * (with appropiate message type).
 */
extern void csim_send_msg(CSIM_MESSAGE *msg,
                          CSIM_ULONG msg_type,
                          CSIM_PROCESS *receiver);

/*
 * Starts the receiving of a message. The call to this macro blocks the process
 * until a message is received. The message is written into the "r_msg"
 * argument, which is pointer to a message. In the long_jump implementation of
 * C-Sim the actual argument cannot be a local variable as the pointer would be
 * destroyed. The "r_sender" parameter specifies the process from which message
 * may be received. If "r_sender" is CSIM_ANY_MSG_SENDER, the message may be
 * received from any process. The "r_msg_type" argument specifies which types
 * of message can be received and is explained in the previous function.
 */
#define csim_receive_msg(r_msg, r_msg_type, r_sender)                         \
do {                                                                          \
  CSIM_ULONG _r_msg_type;                                                     \
  CSIM_PROCESS *_r_sender;                                                    \
  CSIM_BOOLEAN _in_msg_queue = FALSE;                                         \
  CSIM_MESSAGE *_p_m;                                                         \
                                                                              \
  _r_msg_type = (r_msg_type);                                                 \
  _r_sender   = (r_sender);                                                   \
                                                                              \
  if ((_p_m = (CSIM_MESSAGE *) csim_first(csim_msg_queue_point())) != NULL) { \
    do {                                                                      \
      if (csim_test_msg(_p_m, _r_msg_type, _r_sender, csim_current())) {      \
        csim_out((CSIM_LINK *) _p_m);                                         \
        (CSIM_MESSAGE *)(r_msg) = _p_m;                                       \
        _in_msg_queue = TRUE;                                                 \
      }                                                                       \
      else                                                                    \
        _p_m = (CSIM_MESSAGE *) _p_m->suc;                                    \
    } while ((_in_msg_queue == FALSE)                                         \
      && (_p_m != (CSIM_MESSAGE *) csim_first(csim_msg_queue_point())));      \
  }                                                                           \
  if (_in_msg_queue == FALSE) {                                               \
    CSIM_WAITING_PROCESS *_p_wp;                                              \
                                                                              \
    _p_wp = (CSIM_WAITING_PROCESS *) csim_new_link(CSIM_WAITING_PROCESS);     \
    _p_wp->msg_type = _r_msg_type;                                            \
    _p_wp->sender   = _r_sender;                                              \
    _p_wp->receiver = csim_current();                                         \
    _p_wp->p_msg    = (CSIM_MESSAGE **) &(r_msg);                             \
    csim_into((CSIM_LINK *) _p_wp, csim_wp_queue_point());                    \
    csim_passivate();                                                         \
  }                                                                           \
} while(0)


/* For internal use only - DO NOT USE */
CSIM_BOOLEAN csim_test_msg(CSIM_MESSAGE *msg,
                           CSIM_ULONG msg_type,
                           CSIM_PROCESS *sender,
                           CSIM_PROCESS *receiver);

/* For internal use only - DO NOT USE */
extern CSIM_HEAD *csim_msg_queue_point();
extern CSIM_HEAD *csim_wp_queue_point();

#endif
