/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.02.2002

                             C-Sim version 4.2

                              file csim_sem.c

                              C-Sim semaphores

*****************************************************************************/

#include "csim_sem.h"


CSIM_SEMAPHORE *csim_init_semaphore(CSIM_SEMAPHORE *p_sem, CSIM_UWORD cnt)
{
  if (p_sem != NULL) {
    csim_init_head((CSIM_HEAD *) p_sem);
    p_sem->count = cnt;
  }
  return p_sem;
}

void csim_unlock_sem(CSIM_SEMAPHORE *p_sem)
{
  CSIM_LINK *p_first_process;
  if (csim_empty((CSIM_HEAD *) p_sem) == FALSE) {
    p_first_process = csim_first((CSIM_HEAD *) p_sem);
    csim_out(p_first_process);
    csim_activate_at((CSIM_PROCESS *)p_first_process, csim_time());
  }
  else
    (p_sem->count)++;
}
