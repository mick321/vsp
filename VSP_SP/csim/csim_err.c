/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_err.c

                            C-Sim error messages

*****************************************************************************/

#include <string.h>
#include <stdio.h>
#include "csim_err.h"

static char error_msg_buffer[80] = "";


static char *check_message[] = {
  "pointed input object is defective",
  "input pointer has wrong value",
  "input pointer has NULL value",
  "pointed process is terminated"
};


static char *error_message[] = {
  "", /* code 0 - no error */
  "Error in csim_set_name(): ",
  "Error in csim_first(): ",
  "Error in csim_last(): ",
  "Error in csim_empty(): ",
  "Error in csim_cardinal(): ",
  "Error in csim_clear(): ",
  "Error in csim_out(): ",
  "Error in csim_into(), p_link: ",
  "Error in csim_into(), p_head: ",
  "Error in csim_follow(), p_what: ",
  "Error in csim_follow(), p_where: ",
  "Error in csim_precede(), p_what: ",
  "Error in csim_precede(), p_where: ",
  "Error in csim_wait(): ",
  "Error in csim_dispose_dyn_mem(): ",
  "Error in csim_dispose_link(): ",
  "Error in csim_dispose_head(): ",
  "Error in csim_dispose_process(): ",
  "Error in csim_state(): ",
  "Error in csim_idle(): ",
  "Error in csim_evtime(): ",
  "Error in csim_cancel(): ",
  "Error in csim_activate_at(): ",
  "Error in csim_activate_delay(): ",
  "Error in csim_reactivate_at(): ",
  "Error in csim_reactivate_delay(): ",
  "Error in csim_h_stat_on(): ",
  "Error in csim_h_stat_off(): ",
  "Error in csim_init_h_stat(): ",
  "Error in csim_h_stat_status(): ",
  "Error in csim_h_stat(): ",
  "Error in csim_p_stat_on(): ",
  "Error in csim_p_stat_off(): ",
  "Error in csim_init_p_stat(): ",
  "Error in csim_p_stat_status(): ",
  "Error in csim_p_stat(): ",
  "", /* CSIM_E_ERROR_CODE_SEPARATOR */
  "Error in csim_into(): input object is already in a list",
  "Error in csim_follow(): input object p_what is already in a list",
  "Error in csim_follow(): input object p_where is not in a list",
  "Error in csim_precede(): input object p_what is already in a list",
  "Error in csim_precede(): input object p_where is not in a list",
  "Error in csim_hold(): wrong value of parameter 'del_t'",
  "Error in csim_init_process(): input pointer to the program has NULL value",
  "Error in csim_dispose_process(): pointed csim_process is not terminated",
  "Error in csim_step(): calendar is csim_empty",
  "Error in csim_cancel(): pointed csim_process is not planned",
  "Error in csim_cancel(): pointed csim_process must not be CSIM_ACTIVE",
  "Error in csim_activate_at(): wrong value of parameter 't'",
  "Error in csim_activate_at(): pointed csim_process is already planned",
  "Error in csim_activate_at(): pointed csim_process must not be CSIM_ACTIVE",
  "Error in csim_activate_delay(): wrong value of parameter 'del_t'",
  "Error in csim_activate_delay(): pointed csim_process is already planned",
  "Error in csim_activate_delay(): pointed csim_process must not be CSIM_ACTIVE",
  "Error in csim_reactivate_at(): wrong value of parameter 't'",
  "Error in csim_reactivate_at(): pointed csim_process must not be CSIM_ACTIVE",
  "Error in csim_reactivate_delay(): wrong value of parameter 'del_t'",
  "Error in csim_reactivate_delay(): pointed csim_process must not be CSIM_ACTIVE",
  "Error in csim_step(): cannot create process",
  "User error: " /* CSIM_USER_ERROR_CODE */
};


char *csim_error_msg(CSIM_UWORD code)
{
  int lo = code & 0xFF;
  int hi = code >> 8;
  if (lo < CSIM_USER_ERROR_CODE) {
    strcpy(error_msg_buffer, error_message[lo]);
    if (lo < CSIM_E_ERROR_CODE_SEPARATOR) {
      if (hi <= CSIM_P_IS_TERMINATED) {
        strcat(error_msg_buffer, check_message[hi]);
      }
      else {
        strcpy(error_msg_buffer, "Wrong error code");
      }
    }
  }
  else {
    sprintf(error_msg_buffer, "%s%d", error_message[CSIM_USER_ERROR_CODE], hi);
  }
  return error_msg_buffer;
}
