/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                01.04.2003

                             C-Sim version 5.1

                                file csim.c

                               C-Sim library

*****************************************************************************/

#include <string.h>
#include "csim.h"

/********************/
/* global variables */
/********************/

/* counter of simulation steps */
GLOBAL unsigned long int csim_step_number = 0;

/********************/
/* local variables  */
/********************/

LOCAL CSIM_BOOLEAN in_step    = FALSE;         /* csim_step() activity flag */
LOCAL CSIM_BOOLEAN is_error   = FALSE;         /* error flag                */
LOCAL CSIM_BOOLEAN statistics = FALSE;         /* statistics switch         */
LOCAL CSIM_PROCESS *current_proc = NULL;       /* active process            */
LOCAL CSIM_PROCESS *sqs = NULL;                /* head of calendar          */
LOCAL CSIM_DYN_MEM *mem = NULL;                /* dyn. mem. - head of list  */
LOCAL CSIM_TIME current_time = CSIM_ZERO_TIME; /* model time                */
LOCAL CSIM_ERROR act_error;                    /* error record              */


/*********************************************/
/* forwarded declarations of local functions */
/*********************************************/

/* inserts object into the list of dyn. memory blocks */
LOCAL void mem_in(CSIM_DYN_MEM *p_dyn_mem);

/* removes object from the list of dyn. memory blocks */
LOCAL void mem_out(CSIM_DYN_MEM *p_dyn_mem);

/* compute head statistics for item insert */
LOCAL void compute_into_hs(CSIM_HEAD *p_head);

/* compute head statistics for item remove */
LOCAL void compute_out_hs(CSIM_HEAD *p_head,
                          CSIM_LINK *p_link);

/* compute process statistics for activate */
LOCAL void compute_activate_ps(CSIM_PROCESS *p_process);

/*********************/
/* global functions  */
/* *******************/

GLOBAL void csim_exception(CSIM_UWORD error_code,
                           CSIM_PROCESS *p_process,
                           void *p_void)
{
  act_error.error_code = error_code;
  act_error.p_proc_error = p_process;
  act_error.p_void = p_void;
  is_error = TRUE;
  if (in_step == TRUE) {
    csim_return_to_step();
  }
}


GLOBAL void csim_error(CSIM_ERROR *p_error)
{
  p_error->error_code = act_error.error_code;
  p_error->p_proc_error = act_error.p_proc_error;
  p_error->p_void = act_error.p_void;
  is_error = FALSE;
}


GLOBAL CSIM_BOOLEAN csim_error_status(void)
{
  return is_error;
}


GLOBAL CSIM_RESULT csim_set_name(CSIM_DYN_MEM *p_dyn_mem, char *name)
{
  char *str = NULL;

#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_dyn_mem(p_dyn_mem) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_SET_NAME_DYN_MEM,
      csim_dyn_mem_state(p_dyn_mem)), current_proc, p_dyn_mem);
    return FAILURE;
  }
#endif

  if (p_dyn_mem->name != NULL)
    free(p_dyn_mem->name);
  if (name != NULL) {
    if ((str = (char *) malloc((strlen(name) + 1) * sizeof(char))) != NULL)
      strcpy(str, name);
  }
  p_dyn_mem->name = str;
  return SUCCESS;
}


GLOBAL CSIM_LINK *csim_first(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_FIRST_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return (NULL);
  }
#endif
  return (p_head->p_first);
}


GLOBAL CSIM_LINK *csim_last(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_LAST_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return (NULL);
  }
#endif
  return (p_head->p_last);
}


GLOBAL CSIM_BOOLEAN csim_empty(CSIM_HEAD *p_head)
{
  CSIM_HEAD_CHECK state;

  switch (state = csim_head_state(p_head)) {
    case CSIM_H_EMPTY:
      return (TRUE);
    case CSIM_H_NOT_EMPTY:
      return (FALSE);
    case CSIM_H_DEFECTIVE:
    case CSIM_H_ILLEGAL:
    case CSIM_H_IS_NULL:
      csim_exception(CSIM_ERR_CODE(CSIM_E_EMPTY_HEAD, state),
        current_proc, p_head);
    default:
      break;
  }
  return (TRUE);
}


GLOBAL CSIM_WORD csim_cardinal(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_CARD_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return (-1);
  }
#endif
  return (p_head->lq);
}


GLOBAL CSIM_RESULT csim_clear(CSIM_HEAD *p_head)
{
  CSIM_LINK *p_link;

#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_CLEAR_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return FAILURE;
  }
#endif
  while ((p_link = p_head->p_first) != p_head->p_last) {
    p_link->suc->pred = p_link->pred;
    p_link->pred->suc = p_link->suc;
    p_head->p_first = p_link->suc;

    if ((statistics == TRUE) || (p_head->statistics == TRUE)) {
      p_head->sum_tw = csim_time_add(p_head->sum_tw,
        csim_time_sub(current_time, p_link->enter_time));
      p_link->head->arrival_cnt++;
    }
    p_link->suc = NULL;
    p_link->pred = NULL;
    p_link->head = NULL;
  }
  if (p_head->p_first != NULL) {
    p_head->p_first = NULL;
    p_head->p_last = NULL;

    compute_out_hs(p_head, p_link);
    p_head->last_time = current_time;

    p_link->suc = NULL;
    p_link->pred = NULL;
    p_link->head = NULL;
    p_head->lq = 0;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_out(CSIM_LINK *p_link)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_link(p_link) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_OUT_LINK, csim_link_state(p_link)),
      current_proc, p_link);
    return FAILURE;
  }
#endif
  if (p_link->suc != NULL) {
    p_link->suc->pred = p_link->pred;
    p_link->pred->suc = p_link->suc;
    if (p_link == p_link->head->p_first) {
      if (p_link == p_link->head->p_last) {
        p_link->head->p_first = NULL;
        p_link->head->p_last = NULL;
      }
      else {
        p_link->head->p_first = p_link->suc;
      }
    }
    else {
      if (p_link == p_link->head->p_last)
        p_link->head->p_last = p_link->pred;
    }

    compute_out_hs(p_link->head, p_link);
    p_link->head->last_time = current_time;
    p_link->head->lq--;

    p_link->suc = NULL;
    p_link->pred = NULL;
    p_link->head = NULL;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_into(CSIM_LINK *p_link,
                             CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_link(p_link) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_INTO_LINK, csim_link_state(p_link)),
      current_proc, p_link);
    return FAILURE;
  }
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_INTO_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return FAILURE;
  }
#endif
  if ((p_link->suc == NULL) && (p_link->pred == NULL)) {
    if ((p_head->p_first == NULL) && (p_head->p_last == NULL)) {
      p_head->p_first = p_link;
      p_head->p_last = p_link;
      p_link->pred = p_link;
      p_link->suc = p_link;
      p_link->head = p_head;

      compute_into_hs(p_head);
      p_link->head->last_time = current_time;
      p_link->enter_time = current_time;
      p_head->lq = 1;
    }
    else {
      p_link->suc = p_head->p_first;
      p_link->pred = p_head->p_last;
      p_head->p_last->suc = p_link;
      p_head->p_first->pred = p_link;
      p_link->head = p_head;
      p_head->p_last = p_link;

      compute_into_hs(p_head);
      p_link->head->last_time = current_time;
      p_link->enter_time = current_time;
      p_head->lq++;
    }
  }
  else {
    csim_exception(CSIM_E_INTO_LINK_IS_IN, current_proc, p_link);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_follow(CSIM_LINK *p_link_what,
                               CSIM_LINK *p_link_where)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_link(p_link_what) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_FOLL_WHAT,
      csim_link_state(p_link_what)), current_proc, p_link_what);
    return FAILURE;
  }
  if (csim_check_link(p_link_where) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_FOLL_WHERE,
      csim_link_state(p_link_where)), current_proc, p_link_where);
    return FAILURE;
  }
#endif
  if ((p_link_what->suc == NULL) && (p_link_where->head != NULL)) {
    p_link_what->pred = p_link_where;
    p_link_what->suc = p_link_where->suc;
    p_link_what->head = p_link_where->head;
    p_link_what->suc->pred = p_link_what;
    p_link_what->pred->suc = p_link_what;
    if (p_link_where->head->p_last == p_link_where) {
      p_link_where->head->p_last = p_link_what;
    }

    compute_into_hs(p_link_what->head);
    p_link_what->head->last_time = current_time;
    p_link_what->enter_time = current_time;
    p_link_what->head->lq++;

  }
  else {
    if (p_link_what->suc != NULL) {
      csim_exception(CSIM_E_FOLL_WHAT_IS_IN, current_proc, p_link_what);
      return FAILURE;
    }
    if (p_link_where->head == NULL) {
      csim_exception(CSIM_E_FOLL_WHERE_NOT_IN, current_proc, p_link_where);
      return FAILURE;
    }
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_precede(CSIM_LINK *p_link_what,
                                CSIM_LINK *p_link_where)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_link(p_link_what) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_FOLL_WHAT,
      csim_link_state(p_link_what)), current_proc, p_link_what);
    return FAILURE;
  }
  if (csim_check_link(p_link_where) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_FOLL_WHERE,
      csim_link_state(p_link_where)), current_proc, p_link_where);
    return FAILURE;
  }
#endif
  if ((p_link_what->suc == NULL) && (p_link_where->head != NULL)) {
    p_link_what->suc = p_link_where;
    p_link_what->pred = p_link_where->pred;
    p_link_what->pred->suc = p_link_what;
    p_link_what->suc->pred = p_link_what;
    p_link_what->head = p_link_where->head;
    if (p_link_where->head->p_first == p_link_where)
      p_link_where->head->p_first = p_link_what;

    compute_into_hs(p_link_what->head);
    p_link_what->head->last_time = current_time;
    p_link_what->enter_time = current_time;
    p_link_what->head->lq++;
  }
  else {
    if (p_link_what->suc != NULL) {
      csim_exception(CSIM_E_PREC_WHAT_IS_IN, current_proc, p_link_what);
      return FAILURE;
    }
    if (p_link_where->head == NULL) {
      csim_exception(CSIM_E_PREC_WHERE_NOT_IN, current_proc, p_link_where);
      return FAILURE;
    }
  }
  return SUCCESS;
}


GLOBAL void csim_stat_on(void)
{
  statistics = TRUE;
}

GLOBAL void csim_stat_off(void)
{
  statistics = FALSE;
}

GLOBAL CSIM_BOOLEAN csim_stat_status(void)
{
  return statistics;
}


GLOBAL CSIM_RESULT csim_h_stat_on(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_HSTAT_ON_HEAD,
      csim_head_state(p_head)), current_proc, p_head);
    return FAILURE;
  }
#endif
  p_head->statistics = TRUE;
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_h_stat_off(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_HSTAT_OFF_HEAD,
      csim_head_state(p_head)), current_proc, p_head);
    return FAILURE;
  }
#endif
  p_head->statistics = FALSE;
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_init_h_stat(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_HSTAT_INIT_HEAD,
      csim_head_state(p_head)), current_proc, p_head);
    return FAILURE;
  }
#endif
  p_head->arrival_cnt = 0;
  p_head->sum_lw = csim_zero_time();
  p_head->sum_tw = csim_zero_time();
  p_head->sum_time = csim_zero_time();
  p_head->last_time = current_time;
  return SUCCESS;
}


GLOBAL CSIM_BOOLEAN csim_h_stat_status(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_HSTAT_ST_HEAD,
      csim_head_state(p_head)), current_proc, p_head);
    return FALSE;
  }
#endif
  return (p_head->statistics);
}


GLOBAL CSIM_RESULT csim_h_stat(CSIM_HEAD *p_head,
                               double *p_Lw,
                               double *p_Tw)
{
  double d;

#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_HSTAT_HEAD, csim_head_state(p_head)),
      current_proc, p_head);
    return FAILURE;
  }
#endif
  (*p_Lw) = ((d = csim_time_to_dbl(p_head->sum_time)) > DBL_MIN) ?
    csim_time_to_dbl(p_head->sum_lw) / d : 0.0;
  (*p_Tw) = p_head->arrival_cnt > 0 ?
    csim_time_to_dbl(p_head->sum_tw) / p_head->arrival_cnt : 0.0;
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_p_stat_on(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_PSTAT_ON_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  p_process->statistics = TRUE;
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_p_stat_off(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_PSTAT_OFF_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  p_process->statistics = FALSE;
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_init_p_stat(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_PSTAT_INIT_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  p_process->non_passive_time = csim_zero_time();
  p_process->passive_time = csim_zero_time();
  return SUCCESS;
}


GLOBAL CSIM_BOOLEAN csim_p_stat_status(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_PSTAT_ST_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FALSE;
  }
#endif
  return p_process->statistics;
}


GLOBAL CSIM_RESULT csim_p_stat(CSIM_PROCESS *p_process,
                               double *p_Ts,
                               double *p_Tr)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_PSTAT_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  (*p_Ts) = csim_time_to_dbl(p_process->non_passive_time);
  (*p_Tr) = csim_time_to_dbl(p_process->passive_time);
  return SUCCESS;
}


GLOBAL void csim_rank(CSIM_PROCESS *p_process)
{
  CSIM_PROCESS *p_pom;

  if (sqs->suc_in_sqs == NULL) {
    sqs->pred_in_sqs = p_process;
    sqs->suc_in_sqs = p_process;
    p_process->suc_in_sqs = p_process;
    p_process->pred_in_sqs = p_process;
  }
  else {
    if (csim_time_cmp(sqs->pred_in_sqs->evtime, p_process->evtime) <= 0) {
      sqs->pred_in_sqs->suc_in_sqs = p_process;
      sqs->suc_in_sqs->pred_in_sqs = p_process;
      p_process->pred_in_sqs = sqs->pred_in_sqs;
      p_process->suc_in_sqs = sqs->suc_in_sqs;
      sqs->pred_in_sqs = p_process;
    }
    else {
      p_pom = (sqs->suc_in_sqs);
      while (csim_time_cmp(p_pom->evtime, p_process->evtime) <= 0)
        p_pom = p_pom->suc_in_sqs;
      p_process->suc_in_sqs = p_pom;
      p_process->pred_in_sqs = p_pom->pred_in_sqs;
      p_process->pred_in_sqs->suc_in_sqs = p_process;
      p_process->suc_in_sqs->pred_in_sqs = p_process;
      if (p_pom == sqs->suc_in_sqs)
        sqs->suc_in_sqs = p_process;
    }
  }
}


GLOBAL void csim_sqs_out(CSIM_PROCESS *p_process)
{
  p_process->suc_in_sqs->pred_in_sqs = p_process->pred_in_sqs;
  p_process->pred_in_sqs->suc_in_sqs = p_process->suc_in_sqs;

  if ((p_process == sqs->suc_in_sqs) && (p_process == sqs->pred_in_sqs)) {
    sqs->suc_in_sqs = NULL;
    sqs->pred_in_sqs = NULL;
  }
  else {
    if (p_process == sqs->pred_in_sqs)
      sqs->pred_in_sqs = p_process->pred_in_sqs;
    if (p_process == sqs->suc_in_sqs)
      sqs->suc_in_sqs = p_process->suc_in_sqs;
  }
  p_process->suc_in_sqs = NULL;
  p_process->pred_in_sqs = NULL;
}


GLOBAL CSIM_RESULT csim_step(void)
{
  CSIM_PROC_STATE current_state;

  in_step = TRUE;
  csim_step_number++;
  current_proc = sqs->suc_in_sqs;
  if (current_proc != NULL) {
    current_time = current_proc->evtime;
    current_state = current_proc->state;

    current_proc->state = CSIM_ACTIVE;
    if (current_state == CSIM_NEW_PLANNED) {
      csim_create_process(current_proc);
    }
    else {
      csim_switch_processes(csim_sqs_point(), current_proc);
    }

    in_step = FALSE;
    if (is_error == TRUE) {
      current_proc->state = current_state;
      return FAILURE;
    }
    else {
      if (current_proc->state == CSIM_TERMINATED
          || current_proc->state == CSIM_ZOMBIE) {
        csim_cancel_process(current_proc);
      }
      if (current_proc->state == CSIM_ZOMBIE) {
        csim_dispose_process(current_proc);
      }
      return SUCCESS;
    }
  } 
  else {
    in_step = FALSE;
    csim_exception(CSIM_E_STEP_EMPTY, NULL, NULL);
    return FAILURE;
  }
}


GLOBAL CSIM_TIME csim_time(void)
{
  return (current_time);
}


GLOBAL CSIM_PROCESS *csim_current(void)
{
  return current_proc;
}


GLOBAL CSIM_PROCESS *csim_sqs_point(void)
{
  return (sqs);
}

GLOBAL CSIM_DYN_MEM *csim_mem_point(void)
{
  return (mem);
}


GLOBAL CSIM_PROC_STATE csim_state(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_STATE_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return CSIM_TERMINATED;
  }
#endif
  return (p_process->state);
}


GLOBAL CSIM_BOOLEAN csim_idle(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_IDLE_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return (FALSE);
  }
#endif
  if ((p_process->state == CSIM_PASSIVE)
      || (p_process->state == CSIM_NEW_PASSIVE))
    return (TRUE);
  else
    return (FALSE);
}


GLOBAL CSIM_TIME csim_evtime(CSIM_PROCESS *p_process)
{
  CSIM_PROC_CHECK state;

  switch (state = csim_process_state(p_process)) {
    case CSIM_P_IS_RANK:
      return (p_process->evtime);
    case CSIM_P_NOT_RANK:
    case CSIM_P_DEFECTIVE:
    case CSIM_P_ILLEGAL:
    case CSIM_P_IS_NULL:
    case CSIM_P_IS_TERMINATED:
      csim_exception(CSIM_ERR_CODE(CSIM_E_EVTIME_PROC, state),
        current_proc, p_process);
    default:
      break;
  }
  return csim_zero_time();
}


GLOBAL CSIM_PROCESS *csim_next_proc(void)
{
  CSIM_PROCESS *current;

  if ((current = sqs->suc_in_sqs) != NULL) {
    if (current->state == CSIM_ACTIVE) {
      if (current != current->suc_in_sqs) 
        return current->suc_in_sqs;
    }
    else 
      return current;
  }
  return (NULL);
}


GLOBAL CSIM_RESULT csim_cancel(CSIM_PROCESS *p_process)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_CANCEL_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  if (p_process->state != CSIM_ACTIVE) {
    if (csim_process_state(p_process) == CSIM_P_IS_RANK) {
      csim_sqs_out(p_process);
      if (p_process->state == CSIM_NEW_PLANNED)
        p_process->state = CSIM_NEW_PASSIVE;
      else
        p_process->state = CSIM_PASSIVE;

      csim_compute_deactivate_ps(p_process);
      p_process->last_time = current_time;
    }
    else {
      csim_exception(CSIM_E_CANCEL_NOT_RANK, current_proc, p_process);
      return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_CANCEL_IS_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_activate_at(CSIM_PROCESS *p_process,
                                    CSIM_TIME t)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_ACT_AT_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  if (p_process->state != CSIM_ACTIVE) {
    if (p_process->suc_in_sqs == NULL) {
      if (csim_time_cmp(t, current_time) >= 0) {
        p_process->evtime = t;
        p_process->suc_in_sqs = NULL;
        p_process->pred_in_sqs = NULL;
        csim_rank(p_process);
        if (p_process->state != CSIM_NEW_PASSIVE) {
          p_process->state = CSIM_PLANNED;
        }
        else {
          p_process->state = CSIM_NEW_PLANNED;
        }

        compute_activate_ps(p_process);
        p_process->last_time = current_time;
      }
      else {
        csim_exception(CSIM_E_ACT_AT_T, current_proc, p_process);
        return FAILURE;
      }
    }
    else {
      csim_exception(CSIM_E_ACT_AT_IS_RANK, current_proc, p_process);
      return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_ACT_AT_IS_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_activate_delay(CSIM_PROCESS *p_process,
                                       CSIM_TIME del_t)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_ACT_DEL_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  if (p_process->state != CSIM_ACTIVE) {
    if (p_process->suc_in_sqs == NULL) {
      if (csim_time_cmp(del_t, csim_zero_time()) >= 0) {
        p_process->evtime = csim_time_add(current_time, del_t);
        p_process->suc_in_sqs = NULL;
        p_process->pred_in_sqs = NULL;
        csim_rank(p_process);
        if (p_process->state != CSIM_NEW_PASSIVE) {
          p_process->state = CSIM_PLANNED;
        }
        else {
          p_process->state = CSIM_NEW_PLANNED;
        }

        compute_activate_ps(p_process);
        p_process->last_time = current_time;
      }
      else {
        csim_exception(CSIM_E_ACT_DEL_T, current_proc, NULL);
        return FAILURE;
      }
    }
    else {
      csim_exception(CSIM_E_ACT_DEL_IS_RANK, current_proc, p_process);
      return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_ACT_DEL_IS_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_reactivate_at(CSIM_PROCESS *p_process,
                                      CSIM_TIME t)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_REACT_AT_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  if (p_process->state != CSIM_ACTIVE) {
    if (csim_time_cmp(t, current_time) >= 0) {
      if (csim_process_state(p_process) == CSIM_P_IS_RANK) {
        csim_sqs_out(p_process);
      }
      p_process->evtime = t;
      csim_rank(p_process);
      switch (p_process->state) {
        case CSIM_NEW_PASSIVE:
        case CSIM_NEW_PLANNED:
          p_process->state = CSIM_NEW_PLANNED;
          break;
        default:
          p_process->state = CSIM_PLANNED;
          break;
      }

      compute_activate_ps(p_process);
      p_process->last_time = current_time;
    }
    else {
      csim_exception(CSIM_E_REACT_AT_T, current_proc, NULL);
      return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_REACT_AT_IS_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_reactivate_delay(CSIM_PROCESS *p_process,
                                         CSIM_TIME del_t)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_process(p_process) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_REACT_DEL_PROC,
      csim_process_state(p_process)), current_proc, p_process);
    return FAILURE;
  }
#endif
  if (p_process->state != CSIM_ACTIVE) {
    if (csim_time_cmp(del_t, csim_zero_time()) >= 0) {
      if (csim_process_state(p_process) == CSIM_P_IS_RANK) {
        csim_sqs_out(p_process);
      }
      p_process->evtime = csim_time_add(current_time, del_t);
      csim_rank(p_process);
      switch (p_process->state) {
        case CSIM_NEW_PASSIVE:
        case CSIM_NEW_PLANNED:
          p_process->state = CSIM_NEW_PLANNED;
          break;
        default:
          p_process->state = CSIM_PLANNED;
          break;
      }

      compute_activate_ps(p_process);
      p_process->last_time = current_time;
    }
    else {
      csim_exception(CSIM_E_REACT_DEL_T, current_proc, NULL);
      return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_REACT_DEL_IS_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_DYN_MEM_CHECK csim_dyn_mem_state(CSIM_DYN_MEM *p_dyn_mem)
{
  if (p_dyn_mem != NULL) {
    if (p_dyn_mem->check != (char *) p_dyn_mem) {
      return CSIM_D_ILLEGAL;
    }
    if ((p_dyn_mem->p_suc_mem != NULL) && (p_dyn_mem->p_pred_mem != NULL)
        && (p_dyn_mem->p_head_mem != NULL)) {
      return CSIM_D_IS_RANK;
    }
    if ((p_dyn_mem->p_suc_mem == NULL) && (p_dyn_mem->p_pred_mem == NULL)
        && (p_dyn_mem->p_head_mem == NULL)) {
      return CSIM_D_NOT_RANK;
    }
    return CSIM_D_DEFECTIVE;
  }
  return CSIM_D_IS_NULL;
}


GLOBAL CSIM_BOOLEAN csim_check_dyn_mem(CSIM_DYN_MEM *p_dyn_mem)
{
  switch (csim_dyn_mem_state(p_dyn_mem)) {
    case CSIM_D_IS_RANK:
    case CSIM_D_NOT_RANK:
      return TRUE;
    case CSIM_D_DEFECTIVE:
    case CSIM_D_ILLEGAL:
    case CSIM_D_IS_NULL:
    default:
      return FALSE;
  }
}


GLOBAL CSIM_LINK_CHECK csim_link_state(CSIM_LINK *p_link)
{
  if (p_link != NULL) {
    if (p_link->check != (char *) p_link) {
      return CSIM_L_ILLEGAL;
    }
    if (csim_check_dyn_mem((CSIM_DYN_MEM *) p_link) != TRUE) {
      return CSIM_L_DEFECTIVE;
    }
    if ((p_link->suc == NULL) && (p_link->pred == NULL)
        && (p_link->head == NULL)) {
      return CSIM_L_NOT_RANK;
    }
    if ((p_link->suc != NULL) && (p_link->pred != NULL)
        && (p_link->head != NULL)) {
      return CSIM_L_IS_RANK;
    }
    return CSIM_L_DEFECTIVE;
  }
  return CSIM_L_IS_NULL;
}


GLOBAL CSIM_BOOLEAN csim_check_link(CSIM_LINK *p_link)
{
  switch (csim_link_state(p_link)) {
    case CSIM_L_IS_RANK:
    case CSIM_L_NOT_RANK:
      return TRUE;
    case CSIM_L_DEFECTIVE:
    case CSIM_L_ILLEGAL:
    case CSIM_L_IS_NULL:
    default:
      return FALSE;
  }
}


GLOBAL CSIM_HEAD_CHECK csim_head_state(CSIM_HEAD *p_head)
{
  if (p_head != NULL) {
    if (p_head->check != (char *) p_head) {
      return CSIM_H_ILLEGAL;
    }
    if (csim_check_link((CSIM_LINK *) p_head) != TRUE) {
      return CSIM_H_DEFECTIVE;
    }
    if ((p_head->p_first == NULL) && (p_head->p_last == NULL)) {
      return CSIM_H_EMPTY;
    }
    if ((p_head->p_first != NULL) && (p_head->p_last != NULL)) {
      return CSIM_H_NOT_EMPTY;
    }
    return CSIM_H_DEFECTIVE;
  }
  return CSIM_H_IS_NULL;
}


GLOBAL CSIM_BOOLEAN csim_check_head(CSIM_HEAD *p_head)
{
  switch (csim_head_state(p_head)) {
    case CSIM_H_EMPTY:
    case CSIM_H_NOT_EMPTY:
      return TRUE;
    case CSIM_H_DEFECTIVE:
    case CSIM_H_ILLEGAL:
    case CSIM_H_IS_NULL:
    default:
      return FALSE;
  }
}


GLOBAL CSIM_PROC_CHECK csim_process_state(CSIM_PROCESS *p_process)
{
  if (p_process != NULL) {
    if (p_process->check != (char *) p_process) {
      return CSIM_P_ILLEGAL;
    }
    if (csim_check_link((CSIM_LINK *) p_process) != TRUE) {
      return CSIM_P_DEFECTIVE;
    }
    switch (p_process->state) {
      case CSIM_NEW_PASSIVE:
      case CSIM_PASSIVE:
        if ((p_process->suc_in_sqs == NULL)
            && (p_process->pred_in_sqs == NULL)) {
          return CSIM_P_NOT_RANK;
        }
        return CSIM_P_DEFECTIVE;
      case CSIM_NEW_PLANNED:
      case CSIM_PLANNED:
      case CSIM_ACTIVE:
        if ((p_process->suc_in_sqs != NULL)
            && (p_process->pred_in_sqs != NULL)) {
          return CSIM_P_IS_RANK;
        }
        return CSIM_P_DEFECTIVE;
      case CSIM_TERMINATED:
      case CSIM_ZOMBIE:
        if ((p_process->suc_in_sqs == NULL)
            && (p_process->pred_in_sqs == NULL)) {
          return CSIM_P_IS_TERMINATED;
        }
        return CSIM_P_DEFECTIVE;
    }
    return CSIM_P_DEFECTIVE;
  }
  return CSIM_P_IS_NULL;
}


GLOBAL CSIM_BOOLEAN csim_check_process(CSIM_PROCESS *p_process)
{
  switch (csim_process_state(p_process)) {
    case CSIM_P_IS_RANK:
    case CSIM_P_NOT_RANK:
      return TRUE;
    case CSIM_P_DEFECTIVE:
    case CSIM_P_ILLEGAL:
    case CSIM_P_IS_NULL:
    case CSIM_P_IS_TERMINATED:
    default:
      return FALSE;
  }
}


GLOBAL CSIM_RESULT csim_init_mem(void)
{
  is_error = FALSE;
  in_step  = FALSE;

  if (csim_check_dyn_mem(mem) == TRUE) {
    csim_clear_mem();
  }
  mem = (CSIM_DYN_MEM *) malloc(sizeof(CSIM_DYN_MEM));
  if (mem == NULL) {
    return FAILURE;
  }
  mem->check = (char *) mem;
  mem->name  = NULL;
  mem->type  = 'M';
  mem->p_head_mem = NULL;
  mem->p_suc_mem  = NULL;
  mem->p_pred_mem = NULL;
  mem->destructor = NULL;
  mem->view = NULL;
  csim_set_name((CSIM_DYN_MEM *) mem, "Heap");

  sqs = (CSIM_PROCESS *) malloc(sizeof(CSIM_PROCESS));
  if (sqs == NULL) {
    return FAILURE;
  }
  csim_init_kr(sqs);
  sqs->check = (char *) sqs;
  sqs->name  = NULL;
  csim_set_name((CSIM_DYN_MEM *) sqs, "Calendar");
  sqs->type  = 'P';
  sqs->suc   = NULL;
  sqs->pred  = NULL;
  sqs->head  = NULL;
  sqs->program     = NULL;
  sqs->suc_in_sqs  = NULL;
  sqs->pred_in_sqs = NULL;
  sqs->state  = CSIM_TERMINATED;
  sqs->evtime = csim_zero_time();
  sqs->last_time = csim_zero_time();
  sqs->non_passive_time = csim_zero_time();
  sqs->passive_time = csim_zero_time();
  sqs->statistics   = FALSE;

  current_time = csim_zero_time();
  current_proc = NULL;
  csim_step_number  = 0;

  return SUCCESS;
}


GLOBAL CSIM_DYN_MEM *csim_init_dyn_mem(CSIM_DYN_MEM *p_dyn_mem)
{
  if (p_dyn_mem != NULL) {
    p_dyn_mem->check = (char *) p_dyn_mem;
    p_dyn_mem->name  = NULL;
    p_dyn_mem->type  = 'M';
    p_dyn_mem->p_head_mem = NULL;
    p_dyn_mem->p_suc_mem  = NULL;
    p_dyn_mem->p_pred_mem = NULL;
    p_dyn_mem->destructor = NULL;
    p_dyn_mem->view = NULL;
    mem_in(p_dyn_mem);
    return (p_dyn_mem);
  }
  return NULL;
}


GLOBAL CSIM_LINK *csim_init_link(CSIM_LINK *p_link)
{
  if (p_link != NULL) {
    (void) csim_init_dyn_mem((CSIM_DYN_MEM *) p_link);
    p_link->type = 'L';
    p_link->pred = NULL;
    p_link->suc  = NULL;
    p_link->head = NULL;
    p_link->enter_time = csim_zero_time();
    return (p_link);
  }
  return NULL;
}


GLOBAL CSIM_HEAD *csim_init_head(CSIM_HEAD *p_head)
{
  if (p_head != NULL) {
    (void) csim_init_link((CSIM_LINK *) p_head);
    p_head->type    = 'H';
    p_head->p_first = NULL;
    p_head->p_last  = NULL;
    p_head->lq = 0;
    p_head->arrival_cnt = 0;
    p_head->last_time = current_time;
    p_head->sum_tw = csim_zero_time();
    p_head->sum_lw = csim_zero_time();
    p_head->sum_time   = csim_zero_time();
    p_head->statistics = FALSE;
    return (p_head);
  }
  return NULL;
}


GLOBAL CSIM_PROCESS *csim_init_process(CSIM_PROCESS *p_process,
                                       CSIM_PROC_PROGRAM proc_prog)
{
  if ((p_process != NULL) && (proc_prog != NULL)) {
    (void) csim_init_link((CSIM_LINK *) p_process);
    p_process->type = 'P';
    p_process->program     = proc_prog;
    p_process->suc_in_sqs  = NULL;
    p_process->pred_in_sqs = NULL;
    p_process->state  = CSIM_NEW_PASSIVE;
    p_process->evtime = csim_zero_time();
    p_process->last_time = current_time;
    p_process->non_passive_time = csim_zero_time();
    p_process->passive_time = csim_zero_time();
    p_process->statistics = FALSE;
    return (p_process);
  }
  if (proc_prog == NULL) {
    csim_exception(CSIM_E_INIT_PROC_PROG_NULL, current_proc, NULL);
  }
  return NULL;
}


GLOBAL CSIM_RESULT csim_dispose_dyn_mem(CSIM_DYN_MEM *p_dyn_mem)
{
  CSIM_DYN_MEM_CHECK state;

  switch (state = csim_dyn_mem_state(p_dyn_mem)) {
    case CSIM_D_IS_RANK:
      mem_out(p_dyn_mem);
    case CSIM_D_NOT_RANK:
      if (p_dyn_mem->destructor != NULL) {
        p_dyn_mem->destructor(p_dyn_mem);
      }
      if (p_dyn_mem->name != NULL) {
        free((void *) p_dyn_mem->name);
      }
      free((void *) p_dyn_mem);
      p_dyn_mem = NULL;
      break;
    case CSIM_D_DEFECTIVE:
    case CSIM_D_ILLEGAL:
    case CSIM_D_IS_NULL:
    default:
      csim_exception(CSIM_ERR_CODE(CSIM_E_DISP_DYN_MEM, state),
        current_proc, p_dyn_mem);
      return FAILURE;
  }
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_dispose_link(CSIM_LINK *p_link)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_link(p_link) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_DISP_LINK,
      csim_link_state(p_link)), current_proc, p_link);
    return FAILURE;
  }
#endif
  csim_out(p_link);
  csim_dispose_dyn_mem((CSIM_DYN_MEM *) p_link);
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_dispose_head(CSIM_HEAD *p_head)
{
#ifndef CSIM_ERRCHECK_DISABLE
  if (csim_check_head(p_head) == FALSE) {
    csim_exception(CSIM_ERR_CODE(CSIM_E_DISP_HEAD,
      csim_head_state(p_head)), current_proc, p_head);
    return FAILURE;
  }
#endif
  csim_clear(p_head);
  csim_dispose_link((CSIM_LINK *) p_head);
  return SUCCESS;
}


GLOBAL CSIM_RESULT csim_dispose_process(CSIM_PROCESS *p_process)
{
  CSIM_PROC_CHECK state;

  if (p_process->state != CSIM_ACTIVE) {
    switch (state = csim_process_state(p_process)) {
      case CSIM_P_IS_RANK:
        csim_sqs_out(p_process);
      case CSIM_P_NOT_RANK:
        if (p_process->state != CSIM_NEW_PLANNED && p_process->state != CSIM_NEW_PASSIVE) {
          csim_cancel_process(p_process);
        }
      case CSIM_P_IS_TERMINATED:
        csim_dispose_link((CSIM_LINK *) p_process);
        break;
      case CSIM_P_DEFECTIVE:
      case CSIM_P_ILLEGAL:
      case CSIM_P_IS_NULL:
      default:
        csim_exception(CSIM_ERR_CODE(CSIM_E_DISP_PROC, state),
          current_proc, p_process);
        return FAILURE;
    }
  }
  else {
    csim_exception(CSIM_E_DISP_PROC_ACTIVE, current_proc, p_process);
    return FAILURE;
  }
  return SUCCESS;
}


GLOBAL void csim_clear_mem(void)
{
  CSIM_DYN_MEM *p_tmp = NULL;

  while (mem->p_suc_mem != mem->p_pred_mem) {
    p_tmp = mem->p_suc_mem;
    p_tmp->p_suc_mem->p_pred_mem = p_tmp->p_pred_mem;
    p_tmp->p_pred_mem->p_suc_mem = p_tmp->p_suc_mem;
    mem->p_suc_mem = p_tmp->p_suc_mem;
    switch (p_tmp->type) {
      case 'P':
        csim_dispose_process((CSIM_PROCESS *) p_tmp);
        break;
      case 'L':
        csim_dispose_link((CSIM_LINK *) p_tmp);
        break;
      case 'H':
        csim_dispose_head((CSIM_HEAD *) p_tmp);
        break;
      case 'M':
        csim_dispose_dyn_mem((CSIM_DYN_MEM *) p_tmp);
        break;
    }
  }
  p_tmp = mem->p_suc_mem;

  if (p_tmp != NULL) {
    switch (p_tmp->type) {
      case 'P':
        csim_dispose_process((CSIM_PROCESS *) p_tmp);
        break;
      case 'L':
        csim_dispose_link((CSIM_LINK *) p_tmp);
        break;
      case 'H':
        csim_dispose_head((CSIM_HEAD *) p_tmp);
        break;
      case 'M':
        csim_dispose_dyn_mem((CSIM_DYN_MEM *) p_tmp);
        break;
    }
  }
  free((CSIM_DYN_MEM *) mem);
  free((CSIM_PROCESS *) sqs);
  mem = NULL;
  sqs = NULL;
}


GLOBAL void csim_compute_deactivate_ps(CSIM_PROCESS *p_process)
{
  if ((statistics == TRUE) || (p_process->statistics == TRUE))
    p_process->non_passive_time = csim_time_add(p_process->non_passive_time,
      csim_time_sub(current_time, p_process->last_time));
}


/********************/
/* local functions  */
/* ******************/


LOCAL void compute_activate_ps(CSIM_PROCESS *p_process)
{
  if ((statistics == TRUE) || (p_process->statistics == TRUE))
    p_process->passive_time = csim_time_add(p_process->passive_time,
      csim_time_sub(current_time, p_process->last_time));
}


LOCAL void compute_into_hs(CSIM_HEAD *p_head)
{
  CSIM_TIME del_t;

  if ((statistics == TRUE) || (p_head->statistics == TRUE)) {
    del_t = csim_time_sub(current_time, p_head->last_time);
    p_head->sum_lw = csim_time_add(p_head->sum_lw,
      csim_time_mul(del_t, dbl_to_csim_time(p_head->lq)));
    p_head->sum_time = csim_time_add(p_head->sum_time, del_t);
  }
}


LOCAL void compute_out_hs(CSIM_HEAD *p_head, CSIM_LINK *p_link)
{
  CSIM_TIME del_t;

  if ((statistics == TRUE) || (p_head->statistics == TRUE)) {
    del_t = csim_time_sub(current_time, p_head->last_time);
    p_head->sum_lw = csim_time_add(p_head->sum_lw,
      csim_time_mul(del_t, dbl_to_csim_time(p_head->lq)));
    p_head->sum_time = csim_time_add(p_head->sum_time, del_t);
    p_head->sum_tw = csim_time_add(p_head->sum_tw,
      csim_time_sub(current_time, p_link->enter_time));
    p_head->arrival_cnt++;
  }
}


LOCAL void mem_out(CSIM_DYN_MEM *p_dyn_mem)
{
  p_dyn_mem->p_suc_mem->p_pred_mem = p_dyn_mem->p_pred_mem;
  p_dyn_mem->p_pred_mem->p_suc_mem = p_dyn_mem->p_suc_mem;

  if (p_dyn_mem == mem->p_suc_mem) {
    if (p_dyn_mem == mem->p_pred_mem) {
      mem->p_suc_mem = NULL;
      mem->p_pred_mem = NULL;
    }
    else {
      mem->p_suc_mem = p_dyn_mem->p_suc_mem;
    }
  }
  else {
    if (p_dyn_mem == mem->p_pred_mem) {
      mem->p_pred_mem = p_dyn_mem->p_pred_mem;
    }
  }
  p_dyn_mem->p_suc_mem = NULL;
  p_dyn_mem->p_pred_mem = NULL;
  p_dyn_mem->p_head_mem = NULL;
}


LOCAL void mem_in(CSIM_DYN_MEM *p_dyn_mem)
{
  if ((mem->p_suc_mem == NULL) && (mem->p_pred_mem == NULL)) {
    mem->p_suc_mem = p_dyn_mem;
    mem->p_pred_mem = p_dyn_mem;
    p_dyn_mem->p_pred_mem = p_dyn_mem;
    p_dyn_mem->p_suc_mem = p_dyn_mem;
    p_dyn_mem->p_head_mem = mem;
  }
  else {
    if ((mem->p_suc_mem != NULL) && (mem->p_pred_mem != NULL)) {
      p_dyn_mem->p_suc_mem = mem->p_suc_mem;
      p_dyn_mem->p_pred_mem = mem->p_pred_mem;
      mem->p_pred_mem->p_suc_mem = p_dyn_mem;
      mem->p_suc_mem->p_pred_mem = p_dyn_mem;
      p_dyn_mem->p_head_mem = mem;
      mem->p_pred_mem = p_dyn_mem;
    }
  }
}
