/*****************************************************************************
            University of West Bohemia, DCS, Pilsen, Czech Republic
                              (c) copyright
                                27.03.2002

                             C-Sim version 5.1

                              file csim_err.h

                    header file of C-Sim error messages

*****************************************************************************/

#ifndef CSIM_ERR_H
#define CSIM_ERR_H 1

#include "csim.h"

/* 
 * Translates the given numerical error code into a human readable text. 
 * The function returns pointer to a staticaly allocated string. 
 */
extern char *csim_error_msg(CSIM_UWORD code);

#endif
