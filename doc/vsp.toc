\select@language {czech}
\contentsline {section}{\numberline {1}Zad\'an\IeC {\'\i }}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Zadan\'a s\IeC {\'\i }\v t SHO}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Ur\v cen\IeC {\'\i } parametr\r u SHO}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Teoretick\'y v\'ypo\v cet charakteristik uzl\r u s\IeC {\'\i }t\v e}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}St\v redn\IeC {\'\i } tok po\v zadavk\r u v~uzlech s\IeC {\'\i }t\v e}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Z\'at\v e\v z uzl\r u s\IeC {\'\i }t\v e}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}St\v redn\IeC {\'\i } po\v cet po\v zadavk\r u v~uzlu}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}St\v redn\IeC {\'\i } d\'elka fronty v~uzlech}{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}St\v redn\IeC {\'\i } doba odezvy uzl\r u s\IeC {\'\i }t\v e}{5}{subsection.2.5}
\contentsline {section}{\numberline {3}Teoretick\'y v\'ypo\v cet charakteristik cel\'e s\IeC {\'\i }t\v e}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}St\v redn\IeC {\'\i } po\v cet po\v zadavk\r u v~s\IeC {\'\i }ti}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}St\v redn\IeC {\'\i } doba odezvy s\IeC {\'\i }t\v e}{6}{subsection.3.2}
\contentsline {section}{\numberline {4}Simulace SHO}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Objektov\'e programov\'an\IeC {\'\i } v~jazyce~C}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Objektov\'a anal\'yza}{7}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Po\v zadavek}{7}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Generick\'y odkaz}{7}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Statistika}{7}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}Zdroj po\v zadavk\r u}{8}{subsubsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.5}Stok po\v zadavk\r u}{8}{subsubsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.6}Fronta}{9}{subsubsection.4.2.6}
\contentsline {subsubsection}{\numberline {4.2.7}Kan\'al obsluhy}{9}{subsubsection.4.2.7}
\contentsline {subsubsection}{\numberline {4.2.8}K\v ri\v zovatka}{9}{subsubsection.4.2.8}
\contentsline {subsection}{\numberline {4.3}Z\IeC {\'\i }skan\'e statistiky}{9}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Charakteristiky uzl\r u}{10}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Pr\r uchod po\v zadavk\r u celou s\IeC {\'\i }t\IeC {\'\i }}{10}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Sledov\'an\IeC {\'\i } statistik fronty Q$_4$}{10}{subsubsection.4.3.3}
\contentsline {subsection}{\numberline {4.4}Srovn\'an\IeC {\'\i }}{11}{subsection.4.4}
\contentsline {section}{\numberline {5}Simulace SHO s~gaussovsk\'ym rozd\v elen\IeC {\'\i }m pravd\v epodobnosti}{11}{section.5}
\contentsline {subsection}{\numberline {5.1}Gener\'ator n\'ahodn\'ych \v c\IeC {\'\i }sel s~gaussovsk\'ym rozd\v elen\IeC {\'\i }m}{11}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Ov\v e\v ren\IeC {\'\i } funk\v cnosti}{11}{subsubsection.5.1.1}
\contentsline {subsection}{\numberline {5.2}Z\IeC {\'\i }skan\'e statistiky}{12}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Charakteristiky uzl\r u}{12}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Pr\r uchod po\v zadavk\r u celou s\IeC {\'\i }t\IeC {\'\i }}{12}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Sledov\'an\IeC {\'\i } statistik fronty Q$_4$}{12}{subsubsection.5.2.3}
\contentsline {subsection}{\numberline {5.3}Anal\'yza v\'ysledk\r u}{12}{subsection.5.3}
\contentsline {section}{\numberline {6}Z\'av\v er}{13}{section.6}
