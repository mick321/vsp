%-------------------------------------------------------------------------------
% T��da pro psan� dokumentace k semestr�ln� pr�ci
% v 0.1 (9.2009)
% pvanecek@kiv.zcu.cz
%-------------------------------------------------------------------------------
\ProvidesClass{zcu_sp}

\LoadClass[12pt,a4paper]{article}

\usepackage[cp1250]{inputenc}
\usepackage{epsf, graphicx, xcolor}
\usepackage[czech]{babel}

\renewcommand\refname{Literatura}

%-------------------------------------------------------------------------------
% P�id�n� p��kaz� pro p�edm�t a email
%-------------------------------------------------------------------------------
\makeatletter
	\newcommand{\course}[1]{\def \@course {#1}}
	\newcommand{\email}[1]{\def \@email {#1}}
    \newcommand{\narozen}[1]{\def \@narozen {#1}}
	\course{}
	\email{}
    \narozen{}
\makeatother
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Nastaven� velikosti str�nky
%-------------------------------------------------------------------------------
\addtolength{\hoffset}{-1in}
\addtolength{\textwidth}{2cm}
\addtolength{\evensidemargin}{1.5cm}
\addtolength{\oddsidemargin}{1.5cm}
\addtolength{\voffset}{-1in}
\addtolength{\textheight}{2cm}
\addtolength{\topmargin}{1.5cm}
\addtolength{\headheight}{.3cm}
%-------------------------------------------------------------------------------


%-------------------------------------------------------------------------------
% Fonty pro nadpisy + styl popisk�
%-------------------------------------------------------------------------------
\font\fntt = cmsltt10 scaled 1200
\font\ssbfl = csssbx10 scaled 1440
\font\ssbf = csssbx10 scaled 1200

\makeatletter

\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\ssbfl}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\tiny\ssbf}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\ssbf}}


\usepackage[hang,small,bf]{caption}

\makeatother
%-------------------------------------------------------------------------------

%-------------------------------------------------------------------------------
% Z�hlav� a z�pat� pomoc� fancyhdr
%-------------------------------------------------------------------------------
\makeatletter
	\usepackage{fancyhdr}
	\pagestyle{fancy}
	\renewcommand{\footrulewidth}{\headrulewidth}
	\fancyhf{}

	\newcommand{\updateheaders}{		
		\ifx \@author \@empty
			\relax
		\else
			\fancyhead[R]{\ssbf\@author}
		\fi

		\ifx \@course \@empty
			\relax
		\else
			\fancyhead[L]{\ssbf\@course}
		\fi
	}

	\fancyfoot[R]{\ssbf\thepage}
\makeatother
%-------------------------------------------------------------------------------


%-------------------------------------------------------------------------------
% Sazba zdrojov�ch k�d� pomoc� lstlistings + definice jazyka GLSL
% mo�nost p�idat dal�� jazyky dle pot�eby
%-------------------------------------------------------------------------------
\makeatletter
\usepackage{listings}
\lstdefinelanguage{glsl}{
	morecomment = [l]{//}, 
	morecomment = [s]{/*}{*/},
	morekeywords= {attribute, const, uniform, varying, layout, centroid, flat, smooth, noperspective, break, continue, do, for, while, switch, case, default, if, else, in, out, inout, float, int, void, bool, true, false, invariant, discard, return, mat2, mat3, mat4, mat2x2, mat2x3, mat2x4, mat3x2, mat3x3, mat3x4, mat4x2, mat4x3, mat4x4, vec2, vec3, vec4, ivec2, ivec3, ivec4, bvec2, bvec3, bvec4, uint, uvec2, uvec3, uvec4, lowp, mediump, highp, precision, sampler1D, sampler2D, sampler3D, samplerCube, sampler1DShadow, sampler2DShadow, samplerCubeShadow, sampler1DArray, sampler2DArray, sampler1DArrayShadow, sampler2DArrayShadow, isampler1D, isampler2D, isampler3D, isamplerCube, isampler1DArray, isampler2DArray, usampler1D, usampler2D, usampler3D, usamplerCube, usampler1DArray, usampler2DArray, sampler2DRect, sampler2DRectShadow, isampler2DRect, usampler2DRect, samplerBuffer, isamplerBuffer, usamplerBuffer, sampler2DMS, isampler2DMS, usampler2DMS, sampler2DMSArray, isampler2DMSArray, usampler2DMSArray, struct},
	morekeywords= [2]{gl_VertexID, gl_InstanceID, gl_PerVertex, gl_Position, gl_PointSize, gl_ClipDistance, gl_ClipVertex, gl_Color, gl_SecondaryColor, gl_Normal, gl_Vertex, gl_MultiTexCoord0, gl_MultiTexCoord1, gl_MultiTexCoord2, gl_MultiTexCoord3, gl_MultiTexCoord4, gl_MultiTexCoord5, gl_MultiTexCoord6, gl_MultiTexCoord7, gl_FogCoord, gl_MaxVertexAttribs, gl_MaxVertexUniformComponents, gl_MaxVaryingFloats, gl_MaxVaryingComponents, gl_MaxVertexOutputComponents, gl_MaxGeometryInputComponents, gl_MaxGeometryOutputComponents, gl_MaxFragmentInputComponents, gl_MaxVertexTextureImageUnits, gl_MaxCombinedTextureImageUnits, gl_MaxTextureImageUnits, gl_MaxFragmentUniformComponents, gl_MaxDrawBuffers, gl_MaxClipDistances, gl_MaxGeometryTextureImageUnits, gl_MaxGeometryOutputVertices, gl_MaxGeometryTotalOutputComponents, gl_MaxGeometryUniformComponents, gl_MaxGeometryVaryingComponents, gl_MaxTextureUnits, gl_MaxTextureCoords, gl_MaxClipPlanes, gl_DepthRangeParameters, gl_ModelViewMatrix, gl_ProjectionMatrix, gl_ModelViewProjectionMatrix, gl_TextureMatrix, gl_NormalMatrix, gl_ModelViewMatrixInverse, gl_ProjectionMatrixInverse, gl_ModelViewProjectionMatrixInverse, gl_TextureMatrixInverse, gl_ModelViewMatrixTranspose, gl_ProjectionMatrixTranspose, gl_ModelViewProjectionMatrixTranspose, gl_TextureMatrixTranspose, gl_ModelViewMatrixInverseTranspose, gl_ProjectionMatrixInverseTranspose, gl_ModelViewProjectionMatrixInverseTranspose, gl_TextureMatrixInverseTranspose, gl_NormalScale, gl_ClipPlane, gl_Point, gl_FrontMaterial, gl_BackMaterial, gl_LightSource, gl_LightModel, gl_FrontLightModelProduct, gl_BackLightModelProduct, gl_FrontLightProduct, gl_BackLightProduct, gl_TextureEnvColor, gl_EyePlaneS, gl_EyePlaneT, gl_EyePlaneR, gl_EyePlaneQ, gl_ObjectPlaneS, gl_ObjectPlaneT, gl_ObjectPlaneR, gl_ObjectPlaneQ, gl_Fog, gl_FragCoord, gl_FrontFacing, gl_ClipDistance, gl_PointCoord, gl_PrimitiveID, gl_FragDepth, gl_FragColor}
}

\lstset{
        tabsize=2,
        basicstyle=\ttfamily\footnotesize,
        keywordstyle=\color{blue},
        keywordstyle=[2]\color{teal},
        commentstyle=\color{gray},
        stringstyle=\color{green},
        showstringspaces=false,
        captionpos=b
}
\renewcommand\lstlistingname{K�d}
\makeatother
%-------------------------------------------------------------------------------


%-------------------------------------------------------------------------------
% Tituln� str�nka 
%-------------------------------------------------------------------------------
\makeatletter
\usepackage{changepage}
\def \maketitle {%
	\changepage{3cm}{2cm}{-1cm}{-1cm}{}{-1cm}{}{}{}
	\begin{titlepage}
	        \begin{center}
			\hbox to \textwidth{\epsfysize=32mm\raise 6.3mm\hbox{\includegraphics[height=32mm]{logo_zcu}}\hfill}

			\vskip 5cm\hrule\vskip 2.2ex
				{\LARGE\sf \@course\\}
				{\Large\sf \@title\\}
			\vskip 1.7ex\hrule
			\vskip 2ex
		\end{center}

		\vfill
		\begin{flushright}
			{\bf \@author\\}
            E-mail: \@email\\
            Datum narozen�: \@narozen\\
			Seps�no dne: \@date\\
		\end{flushright}
	\end{titlepage}

	\changepage{-3cm}{-2cm}{1cm}{1cm}{}{1cm}{}{}{}
	\newpage
	\updateheaders
}
\makeatother
%-------------------------------------------------------------------------------
